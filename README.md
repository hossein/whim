# WHIM
WHIM (White matter HIerarchical Modelling) is a tool for performing fixel-based analysis on diffusion MRI. WHIM can be used to create white matter fixel templates, or for fitting fixels to new subjects using existing templates. WHIM is written in Python and can be run from the command line or imported into other Python programmes.

## Installation
To install WHIM you can run the following command from the command-line:
<pre>
pip install git+https://git.fmrib.ox.ac.uk/hossein/whim.git
</pre>


## How to create a fixel template?
In order to create a new fixel template, the following inputs must be provided:

- A common anatomical reference space, which can be generated from any modality. However, it is recommended to use a template based on multimodal data that can accurately align white matter.
- Diffusion MRI data from a group of subjects represented in FSL format (triplet of data, bvals, bvecs files). It is advisable to use a homogeneous set of subjects, such as those within a similar age range, if the hierarchical model does not account for a nested structure.
- Nonlinear transformations from the single subject diffusion MRI spaces to the reference space.
- A mask file that identifies the white matter region in standard space.
- A [file-tree](https://open.win.ox.ac.uk/pages/fsl/fslpy/fsl.utils.filetree.html) file that outlines the location of the input files.

After the necessary inputs have been prepared, the `whim EM` (see below or help from the commandline) subcommand can be executed with the appropriate arguments to create a template for each number of fibre population. Then `whim template` must be run with the output directories of each model to create the final template that contains the correct number of fibres for each voxel.

The duration of this process will depend on the number of subjects and the computing resources available. If a cluster computing server is used, the creation of the template should take a few hours. On a regular machine, it could take several days. 

It should be noted that creating a new fixel template is only worthwhile if a unique reference space or a distinct group of subjects is employed. Otherwise, it is not required to create a new template, and the existing templates accessible in this repository can be used.

## How to fit fixels to a new subject using a fixel template? 
It is possible to fit fixels to new subjects with already created fixel templates. The required inputs include:
- Diffusion MRI data in FSL format (data, bvals, bvecs files). The diffusion protocol does not need to be identical to that of the subjects used for template creation.
- Nonlinear transformation from the subject's diffusion space to the reference space used for template creation.
- Fixel template directory in the same format created by the `whim template` command.

This process should take less than an hour for each subject on a normal machine. Once fixels are estimated they can be used to run statistical or data driven analysis such as GLM, ICA, etc.

This reposirtory contains a fixel template created from the HCP young adults diffusion dataset in the Oxford Multimodal Template ([OMM-1](https://git.fmrib.ox.ac.uk/cart/oxford-mm-templates)) space. The template includes the mean and variance of the ball and sticks model, as well as the number of fixels present in each voxel.


## Command-line use
To use WHIM from the command-line one can run `whim <sub-command> <arguments>`. The available sub-commands are explained below:
- `whim subject`: Estimates the parameters of the ball and sticks model from diffusion MRI data of single subjects. 
List of arguments:
<pre>
    <b>Required arguments</b>:
        -d, --data  Diffusion MRI image file
        -b, --bvals b-value file in fsl format
        -r, --bvecs Gradient directions file in fsl format
        -m, --mask  Mask indicating which voxels to analyse
        --nf        Number of crossing fibres for each voxel. Either a scalar or an image
        --output    Output directory

    <b>Optional arguments</b>:
        --model     Diffusion model. Any of : ['bas': ball and sticks with one diffusivity parameter (default), 
                                                'bas_diso': ball and sticks with separate d_ax and d_iso].
        --xfm       Transformation from data to mask/prior space (default: identity transform)
        --init      Directory containing initial values of the parameters in the native space 
        --prior     Directory with the prior mean and variance. (same format as produced by `group`)
        --alpha     Hyperparameter for the prior distribution on noise variance. (Default=1)
        -jac        Use jacobians for optimization. 
</pre>



- `whim group`: Estimates group average parameters given single subjects parameter estimates.
List of arguments:
<pre>
    <b>Required arguments</b>:
         --subjdirs [SUBJDIRS ...] List of subject output directories
         --mask         Mask in standard space
         --output       Output directory

    <b>Optional arguments</b>:
         --alpha  Hyperparameter for the variance prior.(Default=1)
</pre>

- `whim EM`: Estimates the parameters of the hierarchical model by iteratively running appropriate `subject` and `group` commands.
List of arguments:
<pre>
    <b>Required arguments</b>:
         --file-tree      Description of input and optionally output data.
         --mask           Mask in standard space
         --output         Output top-level directory

    <b>Optional arguments</b>:
          --model          Diffusion model. Any of : ['bas': ball and sticks with one diffusivity parameter (default), 
                                                      'bas_diso': ball and sticks with separate d_a and d_iso].
          --nf             Number of crossing fibres for each voxel. Either a scalar or an image.
          --iters          Miximum number of iterations (Default=10).
          -jac             Use jacobians for optimization. 
</pre>


To understand the file-tree structure and formatting please read the [File-Tree documentation](https://open.win.ox.ac.uk/pages/ndcn0236/file-tree) and [tree file for WHIM output](https://git.fmrib.ox.ac.uk/hossein/whim/-/blob/main/whim/trees/whim_output.tree). 

- `whim template`: Runs model selection to create a fixel template from multiple model templates.
List of arguments:
<pre>
    <b>Required arguments</b>:
        --modeldirs [MODELDIRS ...] List of model template directories in order (1, 2, 3). The same format as whim group output
        --mask        Mask in standard space
        --output      Output directory

    <b>Optional arguments</b>:
        --criterion   The criterion for model selection. Any of 'BIC'(default), 'AIC', 'LogLikelihood'.
        --thresh      Threshold for the criterion differences between 2-1 and 3-2. Default=(0, 0)
</pre>

- `whim fixel`: Estimates fixels for a new subject given a fixel template.
List of arguments:
<pre>
    <b>Required arguments</b>:
        --template     Template directory containing prior images per parameter.
        --mask         Mask in standard space
        --output [...] Output directory
        --data [...]   Diffusion MRI data
        --bvals [...]  B-value file in fsl format
        --bvecs [...]  Gradient directions file in fsl format
        --xfm [...]    Transformation from data to template space
</pre>
`[...]` denotes that the argument can be provided for multiple subjects with matched ordering in all other arguments.









