import numpy as np


# DTI STUFF


def vec2tens(t):
    """vector -> tensor
    """
    return np.array([[t[0], t[1], t[2]],
                     [t[1], t[3], t[4]],
                     [t[2], t[4], t[5]]])


def dti_desmat(bvecs, bvals):
    """form design matrix for dti fitting
    """
    x, y, z = bvecs.T
    intercept = np.ones(len(x))
    return np.c_[-bvals * x ** 2,
                 -bvals * 2 * x * y,
                 -bvals * 2 * x * z,
                 -bvals * y ** 2,
                 -bvals * 2 * y * x,
                 -bvals * z ** 2,
                 intercept]


def dti_vals(X, bvecs, bvals):
    """fit dti and output some summary measures
    """
    desmat = dti_desmat(bvecs, bvals)
    logX = np.log(np.abs(X))
    tens_logS0 = logX @ np.linalg.pinv(desmat).T
    tens = tens_logS0[:, :-1]
    s0 = np.exp(tens_logS0[:, -1])
    md = (tens[:, 0] + tens[:, 3] + tens[:, 5]) / 3.

    fa = []
    l1 = []
    l2 = []
    l3 = []
    v1 = []
    v2 = []
    v3 = []
    for t in tens:
        T = vec2tens(t)
        l, v = np.linalg.eig(T)
        i = np.argsort(l)
        l1.append(l[i[2]])
        l2.append(l[i[1]])
        l3.append(l[i[0]])
        v1.append(v[:, i[-1]])
        v2.append(v[:, i[-2]])
        v3.append(v[:, i[-3]])
        fa.append(calc_fa(l))
    return s0, md, np.asarray(fa), \
           np.asarray(l1), np.asarray(l2), np.asarray(l3), \
           np.asarray(v1), np.asarray(v2), np.asarray(v3)


def calc_fa(l):
    """calculate FA based on eigenvalues
    """
    lm = np.mean(l)
    return np.sqrt(3. / 2.) * np.sqrt(np.sum((l - lm) ** 2)) / np.sqrt(np.sum(l ** 2))


# SPHERICAL COORDS
def cart2sph(xyz):
    n = np.linalg.norm(xyz, axis=1, keepdims=True)
    n[n == 0] = 1
    xyz = xyz / n
    th = np.arccos(xyz[:, 2])
    st = np.sin(th)
    idx = (st == 0)
    st[idx] = 1
    ph = np.arctan2(xyz[:, 1] / st, xyz[:, 0] / st)
    ph[idx] = 0
    return th, ph


def sph2cart(th, ph):
    return np.array([np.sin(th) * np.cos(ph), np.sin(th) * np.sin(ph), np.cos(th)], ndmin=2).T


def basgen(params, bvecs, bvals):
    """generate ball and stick prediction
    """
    # params = (s0,d,f,th,ph)
    d, s0, f, ph, th = params
    ang = np.sum(bvecs * sph2cart(th, ph).T, axis=1) ** 2
    return s0 * ((1 - f) * np.exp(-d * bvals) + f * np.exp(-d * bvals * ang))


def random_sphere():
    """sample uniformly on the sphere (I think)
    """
    ph = np.random.uniform(0, 2 * np.pi)
    th = np.arccos(np.random.uniform(-1, 1))
    return th, ph


def random_bas_params():
    """random ball and stick params
    """
    s0 = np.random.uniform(0, 100)
    d = np.random.uniform(1, 3)
    f = np.random.uniform(1e-5, 1.)
    th, ph = random_sphere()
    return d, s0, f, ph, th


def compute_mapping(dti_params, bas_params):
    """obtain function that can map DTI->BAS params
    """
    # assume dti_params: s0,md,fa,l1,l2,l3,v1
    # assume bas_params: s0,d,f,th,ph
    # Get models (exclude v1)
    desmat = np.asarray(dti_params[:-1]).T
    # s0  (should be equal for dti/bas but still...)
    s0_beta = np.matmul(np.linalg.pinv(desmat), bas_params[1])
    # d
    d_beta = np.matmul(np.linalg.pinv(desmat), bas_params[0])
    # f
    f_beta = np.matmul(np.linalg.pinv(desmat), bas_params[2])

    # below is the mapping function
    def mapping(dti_p):
        p = np.asarray(dti_p[:-3]).T
        v1 = dti_p[-3]
        s0 = np.matmul(p, s0_beta)
        d = np.matmul(p, d_beta)
        f = np.matmul(p, f_beta)
        th, ph = cart2sph(v1)
        return [d, s0, f, ph, th]

    return mapping


def train_dit2bass(bvals, bvecs, n_samples=1000):
    data = []
    bas_params = []

    for _ in range(n_samples):
        p = random_bas_params()
        bas_params.append(p)
        data.append(basgen(p, bvecs, bvals))
    data = np.asarray(data)
    bas_params = list(np.asarray(bas_params).T)

    # Fit tensor
    dti_params = dti_vals(data, bvecs, bvals)
    # Get the mapping
    mapping = compute_mapping(dti_params, bas_params)
    return mapping


def dti_init(data, mapping, bvals, bvecs):
    """
    estimate ball and sticks initializing parameters using dtifit.
    """
    return mapping(dti_vals(data, bvecs, bvals))


def train_dit2bass_full(bvals, bvecs, n_sticks, n_samples=1000):
    data = []
    bas_params = []

    for _ in range(n_samples):
        p = random_bas_params(n_sticks)
        bas_params.append(p)
        data.append(basgen(p, bvecs, bvals))
    data = np.asarray(data)
    bas_params = list(np.asarray(bas_params).T)

    # Fit tensor
    dti_params = dti_vals(data, bvecs, bvals)
    # Get the mapping
    mapping = compute_mapping(dti_params, bas_params)
    return mapping
