import numpy as np
from scipy.special import gamma
from typing import Callable, Tuple, Union
from whim import utils
from scipy.optimize import minimize, LinearConstraint


class HierarchicalModel:
    def __init__(self,
                 forward_model: Callable,
                 bounds: np.ndarray,
                 jac: Callable = None,
                 angle_idx: Union[list, np.ndarray] = [],
                 noise_prior: Union[Tuple[float, float], None] = None,
                 group_pior: Union[Tuple[float, float], None] = None,
                 linear_constraints: Union[Tuple, None] = None,
                 param_mask: np.array = None
                 ):
        """
        Fits the hierarchical model with EM.

        Args:
            forward_model: the forward model
            bounds: array of parameter bounds (p, 2)
            angle_idx: indices of phi and theta (nf,2)
            noise_prior: alpha, beta parameter of the gamma prior for noise variance
            group_pior: alpha, beta parameter of the gamma prior for group variance
            linear_constraints: A, lb, ub for linear constraints
            param_mask: The mask to keep the parameters (1) in the hierarchy or exclude them (0)
        """
        self.forward_model = forward_model
        self.bounds = np.atleast_2d(bounds)
        self.n_params = self.bounds.shape[0]

        if len(angle_idx):
            self.angle_idx = np.atleast_2d(angle_idx)
        else:
            self.angle_idx = angle_idx

        self.jac = jac
        if noise_prior is None:
            noise_prior = (1, 0)  # default prior on noise std is 1/x
        if group_pior is None:
            group_pior = (-3, 0)  # default prior on group variance is x

        self.a_n = noise_prior[0]
        self.b_n = noise_prior[1]
        self.a_g = group_pior[0]
        self.b_g = group_pior[1]

        if param_mask is None:
            self.param_mask = np.ones(self.n_params, dtype=bool)
        else:
            assert len(param_mask) == self.n_params
            self.param_mask = np.array(param_mask).astype(bool)

        if linear_constraints is None:
            self.linear_constraints = None
        else:
            self.linear_constraints = LinearConstraint(*linear_constraints)

    @classmethod
    def from_diff_model(cls, diff_model, noise_priors=None, group_priors=None, param_mask=None):
        return cls(forward_model=diff_model.forward_model,
                   jac=diff_model.derivatives,
                   bounds=diff_model.param_bounds,
                   angle_idx=diff_model.angle_idx,
                   linear_constraints=diff_model.constraints,
                   noise_prior=noise_priors,
                   group_pior=group_priors,
                   param_mask=param_mask)

    def log_likelihood(self, params, data):
        """
        calculates likelihood given pramaters and data by assuming noise variance has
        (can be used both for single sample or all samples together
        Args:
            params: (n, k)
            data: (n, d)

        Returns likelihood (n,)
        """
        d = data.shape[-1]
        prediction = self.forward_model(params)
        return -(self.a_n + d / 2) * np.log((((prediction - data) ** 2).sum() + 2 * self.b_n) / 2)

    def log_likelihood_jac(self, params, data):
        d = data.shape[-1]
        offset = self.forward_model(params) - data
        return -2 * (self.a_n + d / 2) * (1 / ((offset ** 2).sum() + 2 * self.b_n)) * (self.jac(params) @ offset)

    def log_prior(self, params, nu_g, sigma2_g):
        """
        Estimates log prior given parameters, group mean, group variances
        """
        c_g = np.zeros(len(params))
        c_g[self.param_mask] = utils.norm_log_pdf(params[self.param_mask], mean=nu_g[self.param_mask], sigma2=sigma2_g[self.param_mask])

        if len(self.angle_idx):
            for (phi_idx, th_idx) in self.angle_idx:
                if self.param_mask[phi_idx]:
                    c_g[phi_idx] = utils.watson_pdf_angles_log(params[phi_idx], params[th_idx], nu_g[phi_idx], nu_g[th_idx], sigma2_g[phi_idx])
                    c_g[th_idx] = 0.
        return c_g.sum()

    def log_prior_jac(self, params, nu_g, sigma2_g):
        jac = np.zeros(len(params))
        jac[self.param_mask] = -(params[self.param_mask] - nu_g[self.param_mask]) / (sigma2_g[self.param_mask])

        if len(self.angle_idx):
            for (phi_idx, th_idx) in np.atleast_2d(self.angle_idx):
                if sigma2_g[phi_idx] > 0:
                    cost = np.sin(params[th_idx]) * np.sin(nu_g[th_idx]) * np.cos(params[phi_idx] - nu_g[phi_idx]) + \
                           np.cos(params[th_idx]) * np.cos(nu_g[th_idx])
                    c = 2 * cost / np.tan(np.pi / 2 * sigma2_g[phi_idx])
                    jac[phi_idx] = -c * np.sin(params[th_idx]) * np.sin(nu_g[th_idx]) * np.sin(
                        params[phi_idx] - nu_g[phi_idx])
                    jac[th_idx] = c * (np.cos(params[th_idx]) * np.sin(nu_g[th_idx]) * np.cos(
                        params[phi_idx] - nu_g[phi_idx]) - \
                                       np.sin(params[th_idx]) * np.cos(nu_g[th_idx]))
        return jac

    def log_posterior(self, params, data, nu_g, sigma2_g):
        """
        computes the likelihood function for a single sample given data and group parameters.
        Args:
            params: parameters (k) vector
            data: data (d) vector
            nu_g: group mean (assumes that orientations are rotated to sample space)
            sigma2_g: group variance

        """
        return self.log_likelihood(params, data) + self.log_prior(params, nu_g, sigma2_g)

    def log_posterior_jac(self, params, data, nu_g, sigma2_g):
        return self.log_likelihood_jac(params, data) + self.log_prior_jac(params, nu_g, sigma2_g)

    def log_posterior_full(self, nu_s, nu_g, sigma2_g, data, rot_mats):
        """
            Computes the full posterior P(nu_s, nu_g, sigma2_g| data)
        """

        n_subj = nu_s.shape[0]
        c_d = np.zeros(n_subj)
        for s in range(n_subj):
            rot_g = rotate_to_subj(nu_g, rot_mats[s], self.angle_idx)
            c_d[s] = self.log_posterior(nu_s[s], data[s], rot_g, sigma2_g)
        c_s = utils.log_inv_gamma(sigma2_g[self.param_mask], self.a_g, self.b_g)
        return np.sum(c_d) + np.sum(c_s)

    def fit_mse(self, data, x0=None, parallel=True):
        """
        fit using mean squared error optimization

        Args:
            data: 2d array (n_sample, n_dim)
            x0: initial point (1d, or 2d array)
            parallel: flag to run in parallel across samples.
        """
        n_sample, n_dim = data.shape
        n_params = self.bounds.shape[0]

        if x0 is None:
            x0 = np.ones((n_sample, n_params))

        if x0.ndim == 1:
            x0 = np.tile(x0, (n_sample, 1))

        # MSE parameters for each sample:
        def optimize_sample(s):
            f = lambda x: ((self.forward_model(x) - data[s]) ** 2).sum()
            obj = minimize(fun=f, x0=x0[s], bounds=self.bounds, constraints=self.linear_constraints)
            return obj.x

        nu = utils.run_parallel(optimize_sample, n_sample, debug=not parallel)
        return np.stack(nu, axis=1)

    def mse_variance(self, data, x_mse):
        """
        Calculate variance of mse parameter estimates
        """
        n_sample, dims = data.shape
        n_params = self.bounds.shape[0]

        # MSE parameters for each sample:
        def calc_hessian(s):
            f = lambda x: ((self.forward_model(x) - data[s]) ** 2).sum()
            nu_s = x_mse[s]
            h = utils.hessian(f, nu_s, self.bounds)
            return nu_s, h

        nu, h = utils.run_parallel(calc_hessian, n_sample, debug=True)
        y = np.stack([self.forward_model(t) for t in nu], axis=0)
        res_var = np.var(data - y, axis=1) * data.shape[1]  # check it with Michiel.
        res_var[res_var == 0] = 1  # happens to all 0 data points
        h /= res_var[:, np.newaxis, np.newaxis]
        # important : abs needs to be checked.
        stds = np.abs(np.diagonal(np.linalg.inv(h + 1e-6 * np.eye(n_params)), axis1=-1, axis2=-2)) ** 0.5

        return stds

    def fit_map(self, data, nu_g, sigma2_g, x0, rotation_mats=None, parallel=True, use_jac=False):
        """
        Fits maximum a posteriori estimation for all samples given group mean and variance
        also using the gradients of the forward model mainly to speed up.

        Args:
            grads: callable
            data: data (n_sample, n_dim)
            nu_g: group parameters (n_params) vector or (n_sample, n_params)
            sigma2_g: group variance (n_params) vector  (n_sample, n_params)
            x0: starting point (n_samples, n_params)
            rotation_mats: (n_sample, 3, 3)
            parallel: flag to run in parallel.
            use_jac: use jacobian to optimize the cost function, increases the speed but might be less accurate
        """
        n_samples, n_dim = data.shape
        if x0.ndim == 1:
            x0 = np.tile(x0, (n_samples, 1))

        if rotation_mats is None:
            rotation_mats = np.tile(np.eye(3), (n_samples, 1, 1))

        if nu_g.ndim == 1:
            nu_g = np.tile(nu_g, (n_samples, 1))

        if sigma2_g.ndim == 1:
            sigma2_g = np.tile(sigma2_g, (n_samples, 1))

        def optimize_sample(s):
            nu_g_rot = rotate_to_subj(nu_g[s], rotation_mats[s], self.angle_idx)

            def cost_func(x):
                return -self.log_posterior(x, data[s], nu_g_rot, sigma2_g[s])

            if use_jac:
                def jac(x):
                    return -self.log_posterior_jac(x, data[s], nu_g_rot, sigma2_g[s])

                obj = minimize(fun=cost_func, x0=x0[s], bounds=self.bounds,
                               constraints=self.linear_constraints, jac=jac)
            else:
                obj = minimize(fun=cost_func, x0=x0[s], bounds=self.bounds, constraints=self.linear_constraints)

            return obj.x, -obj.fun

        nu_s, probs = utils.run_parallel(optimize_sample, n_samples, debug=not parallel)

        return nu_s, np.squeeze(probs)

    def fit_full_posterior(self, data, rotation_mats=None, x0=None):
        """
        Computes the parameters by optimizing the full posterior distribution.
        Assumes group distribution is gaussian for all the parameters and group priors are uniform.
        (not working for models with angular variables)

        Args:
            data: 2d array (n_sample, n_dim)
            x0: initial guess
            rotation_mats: Here for consistency

        Returns:
            the solution and the estimated uncertainty.

        """

        n_samples, dims = data.shape
        n_params = self.bounds.shape[0]
        if rotation_mats is None:
            rotation_mats = np.tile(np.eye(3), (n_samples, 1, 1))

        if x0 is None:
            x0 = np.zeros(n_samples * n_params)
        else:
            x0 = np.ravel(x0)

        def cost_func(x):
            nu_s = np.reshape(x, (n_samples, n_params))
            c_d = self.log_likelihood(data, nu_s)
            # do rotations here : not done yet
            c_g = np.log(n_samples * np.var(nu_s, axis=0).sum() + 2 * self.b_g)

            return (self.a_n + dims / 2) * c_d + (self.a_g + (n_samples - 1) / 2) * c_g

        obj = minimize(fun=cost_func, x0=x0, bounds=np.tile(self.bounds, (n_samples, 1)))
        x = np.reshape(obj.x, (n_samples, n_params))

        h = utils.hessian(cost_func, obj.x, np.tile(self.bounds, (n_samples, 1)))
        stds = np.diagonal(np.linalg.inv(h + 1e-6 * np.eye(n_samples * n_params))) ** 0.5
        stds = np.reshape(stds, (n_samples, 2))

        return x, stds

    def fit_em(self, data, rotation_mats=None, x0=None, max_iters=100, parallel=True):
        """
        Fits the hierarchical model with EM.

        Args:
            data: data of size (n_sample, dim)
            rotation_mats: rotation matrices from sample to std space
            x0: initial guess for sample parameters
            max_iters: maximum number of iterations.
            parallel: flag for parallel computation of maximization step.
        Returns:
            nu_s: estimated sample parameters (iterations, sample, p)
            nu_g: estimated group parameters (1, p)
            sigma2_g: variance of group parameters (1, p)
            probs: cost function at each iteration (iterations, 1).

        """

        n_sample, dims = data.shape
        n_params = self.bounds.shape[0]
        if rotation_mats is None:
            rotation_mats = np.tile(np.eye(3), (n_sample, 1, 1))

        probs = np.zeros(max_iters)
        nu_s = np.zeros((max_iters, n_sample, n_params))
        nu_g = np.zeros((max_iters, n_params))
        sigma2_g = np.zeros((max_iters, n_params))

        if x0 is None:
            nu_s[0] = self.fit_mse(data, parallel=parallel)
            sigma2_g[0] = np.var(nu_s[0], axis=0)
        else:
            nu_s[0] = x0
            sigma2_g[0] = np.var(x0)

        nu_g[0] = utils.average_samples(nu_s[0], self.angle_idx)

        t = 0
        change = 1
        while t < max_iters - 1 and change > 0:
            nu_g[t + 1], sigma2_g[t + 1] = average(nu_s[t], self.a_g, self.b_g, self.angle_idx, rotation_mats)
            nu_s[t + 1], _ = self.fit_map(data, nu_g[t + 1], sigma2_g[t + 1], nu_g[t + 1], rotation_mats, parallel)
            probs[t + 1] = self.log_posterior_full(nu_s[t + 1], nu_g[t + 1], sigma2_g[t + 1], data, rotation_mats)

            change = np.linalg.norm(nu_s[t + 1] - nu_s[t]) + \
                     np.linalg.norm(sigma2_g[t + 1] - sigma2_g[t]) + \
                     np.linalg.norm(nu_g[t + 1] - nu_g[t])
            t += 1

        probs, nu_g, nu_s, sigma2_g = probs[:t + 1], nu_g[:t + 1], nu_s[:t + 1], sigma2_g[:t + 1]
        return nu_s, nu_g, sigma2_g, probs

    def compute_uncertainties(self, nu_s, data, nu_g, sigma2_g, rotation_mats):
        """
        Computes uncertainties for em parameter estimates.
        """
        n_sample, n_params = nu_s.shape
        hs = np.zeros((n_sample, n_params, n_params))
        for s in range(n_sample):
            nu_rot = rotate_to_subj(nu_g, rotation_mats[s], self.angle_idx)
            f = lambda x: self.log_posterior(x, data[s], nu_rot, sigma2_g)
            hs[s] = utils.hessian(f, nu_s[s], self.bounds)

        stds = np.abs(np.diagonal(np.linalg.inv(-hs + 1e-6 * np.eye(n_params)), axis1=-2, axis2=-1)) ** 0.5
        return stds

    def ll_const(self, d):
        if self.b_n == 0.0:
            return 0
        else:
            return np.log(((self.b_n ** self.a_n) * gamma(self.a_n + d / 2)) \
                          / (gamma(self.a_n) * (2 * np.pi) ** (d / 2)))

    def calc_bic(self, data, params):
        """
        calculates BIC for a set of samples
        Args:
            data: samples of measurements (n, d)
            params: parameter estimates (n, k)
        Return:
            BIC measure (n,)
        """
        n, d = data.shape
        k = params.shape[-1]
        ll = [self.log_likelihood(p, m) + self.ll_const(d) for m, p in zip(data, params)]
        return k * np.log(d) - 2 * np.array(ll)

    def calc_aic(self, data, params):
        """
        calculates AIC for a set of samples
        Args:
            data: samples of measurements (n, d)
            params: parameter estimates (n, k)
        Return:
            aic measure (n,)
        """
        d = data.shape[-1]
        k = params.shape[-1]
        ll = [self.log_likelihood(p, m) + self.ll_const(d) for m, p in zip(data, params)]

        return k * 2 - 2 * np.array(ll)

    def calc_quality(self, data, params):
        """
        Calculates all the qulaity of fit measures(AIC, BIC, LL) all at once.
        (its more efficient)
        Args:
            data:
            params:

        Returns:
            log-likelihood, aic, bic,
        """
        n, d = data.shape
        k = params.shape[-1]

        ll = np.array([self.log_likelihood(p, m) for m, p in zip(data, params)]) + self.ll_const(d)
        bic = k * np.log(d) - 2 * ll
        aic = k * 2 - 2 * np.array(ll)
        return ll, aic, bic


def average_dyads(nu_s, a_g, b_g):
    """
    computes the group average and variance for angular parameters
    Args:
        nu_s: sample parameter estimates (n_sample, )
        a_g: shape parameter for prior on variance
        b_g: scale parameter for prior on variance
    Returns:
        nu_g: mean
        sigma2_g: variance
    """
    assert nu_s.shape[-1] == 3

    nu_s = nu_s[~np.isnan(nu_s).all(axis=1)]
    nu_s = nu_s[np.linalg.norm(nu_s, axis=-1) > 0]
    if len(nu_s) == 0 or not np.any(nu_s):
        return np.zeros(3) + np.nan, np.zeros(1) + np.nan

    n_sample = nu_s.shape[0]
    vals, vecs = np.linalg.eigh(np.einsum('ik,ip->kp', nu_s, nu_s))
    nu_g = vecs[..., -1]
    func = lambda x: -dispersion_logposterior(x, nu_g, nu_s, a_g, b_g)
    init = 2 / np.maximum(((1 - vals[-1] / n_sample) + 2 * b_g) * (2 * a_g + n_sample + 1), 2)
    bounds = np.array([[1e-2, 0.99]])
    obj = minimize(fun=func, x0=init, bounds=bounds)
    return nu_g, obj.x


def average_scalar(nu_s, a_g, b_g, prior_dist='exp'):
    """
    computes group mean and variance for scalar parameters:
    Args:
        nu_s: sample parameter estimates (n_sample, )
        a_g: shape parameter for prior on variance
        b_g: scale parameter for prior on variance
        prior_dist: either gamma or exponential
    Returns:
        nu_g: mean scalar
        sigma2_g: variance scalar
    """
    nu_s = nu_s[~np.isnan(nu_s)]
    if len(nu_s) == 0:
        return np.nan, np.nan

    n_sample = nu_s.shape[0]
    nu_g = np.mean(nu_s)
    if prior_dist == 'gamma':
        sigma2_g = (((nu_s - nu_g) ** 2).sum(axis=0) / 2 + b_g) / (a_g + n_sample / 2 + 1) # for gamma prior
    elif prior_dist == 'exp':
        sigma2_g = (n_sample - np.sqrt(n_sample ** 2 - 8 * a_g * ((nu_s - nu_g) ** 2).sum())) / (4 * a_g)  # for exponential prior
    else:
        raise ValueError('Group prior distribution is either gamma or exp')
    return nu_g, sigma2_g


def average(nu_s, a_g, b_g, angle_idx, rotation_mats):
    """
    Compute average step in em algorithm, that is computing group average and variance from sample parameters.
    Args:
        nu_s: sample parameters (n_sample, n_params) array
        a_g: parameter of inverse gamma distribution for prior on sigma2_g
        b_g: shape parameter of inverse gamma distribution for prior on sigma2_g
        angle_idx: indices for phi and theta (n,2) array.
        rotation_mats: sample rotation matrices (n_sample, 3, 3) (rotation from native to std space)
    """
    n_samples, n_params = nu_s.shape
    nu_g = np.zeros(n_params)
    sigma2_g = np.zeros(n_params)

    for i in range(n_params):
        if i not in angle_idx:
            nu_g[i], sigma2_g[i] = average_scalar(nu_s[:, i], a_g, b_g)

    if len(angle_idx):
        for i, (phi_idx, th_idx) in enumerate(np.atleast_2d(angle_idx), 1):
            mus_native = utils.p2c(nu_s[:, phi_idx], nu_s[:, th_idx])
            mus = utils.rotate(mus_native, rotation_mats)
            mug, sigma2_g[phi_idx] = average_dyads(mus, a_g, b_g)
            sigma2_g[th_idx] = sigma2_g[phi_idx]
            nu_g[phi_idx], nu_g[th_idx] = utils.c2p(mug)

    return nu_g, sigma2_g


def dispersion_logposterior(odi, mu_g, mu_s, a_g, b_g):
    """
    computes the log posterior for dispersion parameter. Assumes inverse gamma prior with beta=1.
    """
    l = np.log(utils.watson_pdf(mu_s, mu_g, odi)).sum() + utils.log_inv_gamma(odi, a_g, b_g)
    return l


def rotate_to_subj(params, rotmat, angle_idx):
    """
    Rotates angles from group space to subject space.
    Args:
        params: original group parameters (k,)
        rotmat: rotation from "subject" to group (inv is taken)
        angle_idx: index of angles (n, 2) array.
    """
    rot_params = np.copy(params)
    if len(angle_idx):
        for (ph, th) in angle_idx:
            mu = utils.rotate(utils.p2c(params[ph], params[th]), np.linalg.inv(rotmat))
            rot_params[ph], rot_params[th] = utils.c2p(mu)

    return rot_params
