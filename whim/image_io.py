#!/usr/bin/env python3

"""
This module contains functions for reading and writing to image files in different spaces.
"""

import numpy as np
from fsl.data.image import Image
from fsl.transform import fnirt
import subprocess
import fsl.utils.assertions as asrt
from fsl.wrappers import wrapperutils as wutils


def read_image(src, mask, xfm=None):
    """
    sample nearest neighbour voxels from a source image (e.g. native diffusion)
     given a mask in a different space(e.g. std) and the transformation
    from source to mask. if the point lies out of the image returns nan.
    Do not provide xfm if they are in the same space.
    Args:
         src: address of the source image
         mask: address of the mask image
         xfm: transformation (warp field) from source to mask space, None if they are in the same space
    Return:
        data: sampled data (n_vox, n_dim)
        ref_coords: coordinates in the reference space. (n_vox, 3)
        src_coords: coordinates in the source space
    """

    mask_img, src_img = Image(mask), Image(src)
    ref_coords = np.array(np.where(np.nan_to_num(mask_img.data) > 0)).T
    n_vox = ref_coords.shape[0]

    if xfm is None:
        data = src_img.data[tuple(ref_coords.T)]
        src_coords = ref_coords.copy()
    else:
        warp_img = Image(xfm)
        transform = fnirt.readFnirt(fname=warp_img, src=src_img, ref=mask_img, intent=2006)
        src_coords = np.around(transform.transform(ref_coords, 'voxel', 'voxel')).astype(int)
        valid_indices = np.all((src_coords < src_img.shape[:3]) & (src_coords > 0), axis=1)

        if len(src_img.shape) == 3:
            data = np.zeros(n_vox) + np.nan  # set default values to nan
        else:
            data = np.zeros((n_vox, *src_img.shape[3:])) + np.nan

        data[valid_indices] = src_img.data[tuple(src_coords[valid_indices].T)]

    return data, ref_coords, src_coords


class NNMapping:
    """
    Non-linear transform between native and standard space.

    Compute the voxel-wise rotations from src to target given a mask in the target space and the transformation
    from source to mask. This function doesnt load data, it returns functions
    Args:
         src: address of the source image
         mask: address of the mask image
         xfm: transformation (warp field) from source to mask space, None if they are in the same space
    Return:
        mapping object with the methods:
            load_native:
            load_std:
            clean:
            restore:

    """

    def __init__(self, src, mask, xfm):
        self.mask_img, self.src_img = Image(mask), Image(src)
        self.ref_coords = np.array(np.where(np.nan_to_num(self.mask_img.data) > 0)).T
        self.n_vox = self.ref_coords.shape[0]

        if xfm is None:
            self.src_coords = self.ref_coords.copy()
            self.valid_indices = np.ones(self.src_coords.shape[0], dtype=bool)
            self.rot_mats = np.tile(np.eye(3), (self.n_vox, 1, 1))
        else:
            warp_img = Image(xfm)
            transform = fnirt.readFnirt(fname=warp_img, src=self.src_img, ref=self.mask_img, intent=2006)
            self.src_coords = np.around(transform.transform(self.ref_coords, 'voxel', 'voxel')).astype(int)
            self.valid_indices = np.all((self.src_coords < self.src_img.shape[:3]) & (self.src_coords > 0), axis=1)

            dx, dy, dz, _ = warp_img.pixdim
            grads = np.stack(np.gradient(warp_img.data, dx, dy, dz, axis=(0, 1, 2)), axis=-1)
            self.rot_mats = np.zeros((self.n_vox, 3, 3)) + np.eye(3)  # set default vaules to eye(3)
            self.rot_mats[self.valid_indices] = grads[tuple(self.ref_coords[self.valid_indices].T)]

            if transform.deformationType == 'relative':
                self.rot_mats[self.valid_indices] += np.eye(3)

        self.valid_src_coords = self.src_coords[self.valid_indices]
        self.valid_ref_coords = self.ref_coords[self.valid_indices]
        self.valid_rot_mats = self.rot_mats[self.valid_indices]
        self.n_valid_vox = len(self.valid_src_coords)

    def load_native(self, new_img):
        """load data from new image"""
        img = Image(new_img)
        data = img.data[tuple(self.valid_src_coords.T)]
        return data

    def load_std(self, new_img):
        img = Image(new_img)
        data = img.data[tuple(self.valid_ref_coords.T)]
        return data

    def restore(self, data):
        """ gets cleaned data (n_valid_vox, ...) returns full version (n_vox)"""
        if data.ndim == 1:
            shape = self.n_vox
        elif data.ndim == 2:
            shape = (self.n_vox, data.shape[1])
        else:
            shape = (self.n_vox, *data.shape[1:])

        c = np.zeros(shape)
        c[self.valid_indices == 1] = data
        c[self.valid_indices == 0] = np.nan
        return c

    def write_native(self, data, fname):
        """ writes the data to the native space
        Args:
            data: must have n_unique_vox rows
            fname: name of the output file.
        """
        assert data.shape[0] == self.n_valid_vox
        write_nifti(data, self.valid_src_coords, self.src_img, fname)

    def write_std(self, data, fname):
        """
        writes fata to std space
        Args:
            data: must have same rows as the unique voxels.
        """
        assert data.shape[0] == self.n_valid_vox
        write_nifti(self.restore(data), self.ref_coords, self.mask_img, fname)


def read_params(path, param_names, mask, xfm):
    """
    Reads parameter fits from the given path
    """
    x = []
    for p in param_names:
        f_name = f'{path}/{p}_native.nii.gz'
        x.append(read_image(f_name, mask, xfm))

    return np.stack(x, axis=1)


def write_nifti(data: np.ndarray, coords: np.ndarray, ref_img: str, fname: str):
    """
    writes data to a nifti file.

    :param data: data matrix to be written to the file (M, d).
    :param ref_img: address to the reference image in target space (used to define resolution, FOV, etc)
    :param fname: path to the output nifti file
    :param coords: coordinates to write the files.
    :return:
    """
    if data.ndim == 1:
        data = data[:, np.newaxis]

    mask = Image(ref_img)
    if not np.all((coords < mask.shape[:3]) & (coords >= 0)):
        raise ValueError(" coordinated are not matched with the reference space.")
    img = np.zeros((*mask.shape[:3], data.shape[1]))
    img[tuple(coords.T)] = data
    Image(img, header=mask.header).save(fname)


def run_command(cmd: str, time_out=None):
    """
    Runs bash commands and prints the output.
    Args:
        cmd: command :string
        time_out: maximum run time (default inf)
    Returns:
        0 if no error, -1 if there was error
    """
    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                         shell=True, timeout=time_out, universal_newlines=True)
    except subprocess.CalledProcessError as exc:
        print("Status : FAIL", exc.returncode, exc.output)
        return -1
    else:
        print("Output: \n{}\n".format(output))
        return 0


def apply_xfm(img, warp, ref, out, orientation=False):
    """
    Apply transformation to an image using fnirt or vecreg:
    Args:
        img: the input image
        warp: warpfield
        ref: reference image
        out: output address.
        orientation: If true uses vec reg, if false uses fnirt (default)

    """
    if orientation is True:
        vecreg(img, warp, ref, out, interp="nearestneighbour")
    else:
        applywarp(img, warp, ref,  out,)


@wutils.fileOrImage('warp', 'ref')
@wutils.fslwrapper
def invwarp(warp, ref, out, **kwargs):
    """
        Wrapper for fsl invwarp
    """
    valmap = {
        'noconstraint': wutils.SHOW_IF_TRUE,
        'debug': wutils.SHOW_IF_TRUE,
        'verbose': wutils.SHOW_IF_TRUE,
        }

    asrt.assertFileExists(warp, ref)
    asrt.assertIsNifti(warp, ref)
    kwargs.update({'warp': warp, 'out': out, 'ref': ref})
    cmd = ['invwarp'] + wutils.applyArgStyle('--=', valmap=valmap, **kwargs)
    return cmd


@wutils.fileOrImage('warp', 'ref')
@wutils.fileOrArray('affine')
@wutils.fslwrapper
def convertwarp(affine, warp, ref, out, **kwargs):
    """
    wrapper function for convertwarp
    """
    valmap = {
        'constrainj': wutils.SHOW_IF_TRUE,
        'verbose': wutils.SHOW_IF_TRUE,
    }

    asrt.assertFileExists(warp, ref)
    asrt.assertIsNifti(warp, ref)
    kwargs.update({'ref': ref, 'premat': affine, 'warp1': warp, 'out': out})
    cmd = ['convertwarp'] + wutils.applyArgStyle('--=', valmap=valmap, **kwargs)
    return cmd


@wutils.fileOrImage('input', 'ref', 'warpfield')
@wutils.fslwrapper
def vecreg(input, warpfield, ref, output, **kwargs):
    """
    wrapper function for vecreg
    """
    valmap = {}
    asrt.assertFileExists(input, warpfield, ref)
    asrt.assertIsNifti(input, warpfield, ref)
    kwargs.update({'input': input, 'warpfield': warpfield,
                   'output': output, 'ref': ref})
    cmd = ['vecreg'] + wutils.applyArgStyle('--=', valmap=valmap, **kwargs)
    return cmd


@wutils.fileOrImage('input', 'ref', 'warpfield')
@wutils.fslwrapper
def applywarp(input, warpfield, ref, output, **kwargs):
    """
    wrapper function for vecreg
    """
    valmap = {}
    asrt.assertFileExists(input, warpfield, ref)
    asrt.assertIsNifti(input, warpfield, ref)
    kwargs.update({'in': input, 'warp': warpfield,
                   'out': output, 'ref': ref})
    cmd = ['applywarp'] + wutils.applyArgStyle('--=', valmap=valmap, **kwargs)
    return cmd


@wutils.fileOrImage('data', 'mask')
@wutils.fileOrArray('bvecs', 'bvals')
@wutils.fslwrapper
def xfibres(data, mask, bvals, bvecs, nf, logdir, **kwargs):
    """Wrapper for the xfibres command."""

    valmap = {
        'forcedir'                  : wutils.SHOW_IF_TRUE,
        'noard'                     : wutils.SHOW_IF_TRUE,
        'allard'                    : wutils.SHOW_IF_TRUE,
        'nospat'                    : wutils.SHOW_IF_TRUE,
        'nonlinear'                 : wutils.SHOW_IF_TRUE,
        'cnonlinear'                : wutils.SHOW_IF_TRUE,
        'rician'                    : wutils.SHOW_IF_TRUE,
        'f0'                        : wutils.SHOW_IF_TRUE,
        'ardf0'                     : wutils.SHOW_IF_TRUE,
    }

    asrt.assertFileExists(data, mask, bvecs, bvals)
    asrt.assertIsNifti(data, mask)

    kwargs.update({'data'  : data,
                   'mask'  : mask,
                   'bvecs' : bvecs,
                   'bvals' : bvals,
                   'logdir': logdir,
                   'nf' : nf
                  })

    cmd = ['xfibres'] + wutils.applyArgStyle('--=', valmap=valmap, **kwargs)
    return cmd


def nn_sampling_twosided(src, mask, src2mask, mask2src):
    """
    sample nearest neighbour voxels from a source image give a mask and the transformation
    from source to mask. if the point lies out of the image returns nan
    Args:
         src: address of the source image
         mask: address of the mask image
         xfm: transformation (warp field) from source to mask, None if they are in the same space
    Return:
        data: sampled data (n_vox, n_dim)
        valid_indices: binary array indicating voxels that had valid coordinates in the source space. It has n_vox 1s.
        src_coords: coordinates in the source space (n_vox, 3)
        ref_coords: coordinated in the refrence space. (n_all_vox, 3)
    """

    mask_img, src_img = Image(mask), Image(src)
    ref_coords = np.array(np.where(np.nan_to_num(mask_img.data) > 0)).T
    n_vox = ref_coords.shape[0]

    if src2mask is None:
        data = src_img.data[tuple(ref_coords.T)]
        rot_mats = np.tile(np.eye(3), (n_vox, 1, 1))
    else:
        transform = fnirt.readFnirt(fname=Image(src2mask), src=src_img, ref=mask_img, intent=2006)
        src_coords = np.around(transform.transform(ref_coords, 'voxel', 'voxel')).astype(int)
        valid_indices = np.all((src_coords < src_img.shape[:3]) & (src_coords > 0), axis=1)

        if len(src_img.shape) == 3:
            data = np.zeros(n_vox) + np.nan  # set default values to nan
        else:
            data = np.zeros((n_vox, *src_img.shape[3:])) + np.nan

        data[valid_indices] = src_img.data[tuple(src_coords[valid_indices].T)]

        warp_img = Image(src2mask)
        dx, dy, dz, _ = warp_img.pixdim
        grads = np.stack(np.gradient(warp_img.data, dx, dy, dz, axis=(0, 1, 2)), axis=-1)
        rot_mats1 = np.zeros((n_vox, 3, 3)) + np.eye(3)  # set default vaules to eye(3)
        rot_mats1[valid_indices] = grads[tuple(ref_coords[valid_indices].T)]

        warp_img = Image(mask2src)
        dx, dy, dz, _ = warp_img.pixdim
        grads = np.stack(np.gradient(warp_img.data, dx, dy, dz, axis=(0, 1, 2)), axis=-1)
        rot_mats2 = np.zeros((n_vox, 3, 3)) + np.eye(3)  # set default vaules to eye(3)
        rot_mats2[valid_indices] = grads[tuple(src_coords[valid_indices].T)]

        if transform.deformationType == 'relative':
            rot_mats2[valid_indices] += np.eye(3)

    return data, rot_mats1, rot_mats2, ref_coords, src_coords


def nn_sampling(src, mask, xfm=None):  # depricated!
    """
    sample nearest neighbour voxels from a source image (e.g. native diffusion)
     given a mask in a different space(e.g. std) and the transformation
    from source to mask. if the point lies out of the image returns nan
    Args:
         src: address of the source image
         mask: address of the mask image
         xfm: transformation (warp field) from source to mask space, None if they are in the same space
    Return:
        data: sampled data (n_vox, n_dim)
        rots: rotation matrices (from mask 2 src)
        ref_coords: coordinated in the refrence space. (n_vox, 3)
    """

    mask_img, src_img = Image(mask), Image(src)
    ref_coords = np.array(np.where(np.nan_to_num(mask_img.data) > 0)).T
    n_vox = ref_coords.shape[0]

    if xfm is None:
        data = src_img.data[tuple(ref_coords.T)]
        rot_mats = np.tile(np.eye(3), (n_vox, 1, 1))
        src_coords = ref_coords.copy()
    else:
        warp_img = Image(xfm)
        transform = fnirt.readFnirt(fname=warp_img, src=src_img, ref=mask_img, intent=2006)
        src_coords = np.around(transform.transform(ref_coords, 'voxel', 'voxel')).astype(int)
        valid_indices = np.all((src_coords < src_img.shape[:3]) & (src_coords > 0), axis=1)

        if len(src_img.shape) == 3:
            data = np.zeros(n_vox) + np.nan  # set default values to nan
        else:
            data = np.zeros((n_vox, *src_img.shape[3:])) + np.nan
        data[valid_indices] = src_img.data[tuple(src_coords[valid_indices].T)]

        dx, dy, dz, _ = warp_img.pixdim
        grads = np.stack(np.gradient(warp_img.data, dx, dy, dz, axis=(0, 1, 2)), axis=-1)
        rot_mats = np.zeros((n_vox, 3, 3)) + np.eye(3)  # set default vaules to eye(3)
        rot_mats[valid_indices] = grads[tuple(ref_coords[valid_indices].T)]

        if transform.deformationType == 'relative':
            rot_mats[valid_indices] += np.eye(3)

    return data, rot_mats, ref_coords, src_coords


