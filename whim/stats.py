# This module is to calculate statistical significance for fixel based analysis, including permutation testing and TFCE

import numpy as np
from whim import image_io
from itertools import chain
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import connected_components

from scipy.integrate import simpson


def neighbours(c1, c2, o1, o2, angle_th=np.pi / 10):
    """
    Finds the adjacent fixels of a given fixel.
    c1 , c2: coordinates of the fixel and all other fixel (xyz)
    o1, o2: orientation of the fixel and all other fixels (xyz)
    """
    ind = np.argwhere(np.vstack([abs(l - p) <= 1 for l, p in zip(c2.T, c1)]).all(axis=0))[:, 0]
    angles = np.abs(o1.dot(o2[ind].T))
    angle_ne = np.argwhere(angles >= np.cos(angle_th))[:, 0]
    return ind[angle_ne]


def dist(c1, c2, o1, o2):
    # c1, 2: coordinates of fixels; o1, o2: orientations:
    euc = np.linalg.norm(c1 - c2, axis=1)
    cos = np.abs(o1.dot(o2.T))
    return np.array([euc, cos]).T


def calc_zval(data):
    """
    calculates z-value corrected for the number of subjects
    Args:
        data: array of data (n_fix, n_subj)
    """
    return np.abs(data.mean(axis=1) / data.std(axis=1, ddof=1)) * np.sqrt(data.shape[1])


def calc_tval(design, data, contrast):
    """
    Calculate t-values of a regression model:
    Args:
        design: Design matrix, assumed to be normalized and the first column is all ones (n_subj, n_evs)
        data: Data (n_subj, n_fixel) assumed to be normalized column-wise
        contrast: n_contrasts * n_evs
    Returns:
        t-values: n_con x n_fixel
    """
    beta = np.linalg.pinv(design) @ data
    se = np.std(data - design @ beta, ddof=design.shape[1], axis=0)[:, np.newaxis] * \
        np.diag(contrast @ np.linalg.inv(design.T @ design) @ contrast.T) ** 0.5
    no_change = se == 0
    se[no_change] = 1   # needs checking, happens sometime in data
    t_vals = (contrast @ beta) / se.T
    t_vals[no_change.T] = 0
    return t_vals


def load_template_graph(template_dir, mask=None, angle_threshold=np.pi / 5):
    """

    Args:
        template_dir: template directory that contains dyads and n_fibres
        mask: path to the mask in the standard space
        angle_threshold: threshold for angle between fixels for making graph.

    Returns:
        Adjacency matrix of fixels, voxel index, fixel index, fixel locations and fixel orientations.
        Its currently slow, can make it faster
    """
    if mask is None:
        mask = f'{template_dir}/n_fibres.nii.gz'

    n_fibres, coords, _ = image_io.read_image(f'{template_dir}/n_fibres.nii.gz', mask)
    valid_voxs = n_fibres > 0

    n_fibres = n_fibres[valid_voxs]
    n_vox = valid_voxs.sum()
    coords = coords[valid_voxs]

    dyads = np.zeros((3, n_vox, 3))
    for i in range(3):
        dyads[i] = image_io.read_image(f'{template_dir}/dyads_{i + 1}_mean.nii.gz', mask)[0][valid_voxs]
        dyads[i, i >= n_fibres, :] = 0

    all_orientations = np.vstack(dyads)
    valid_fixs = abs(all_orientations).sum(axis=1) > 0
    vox_ind = np.hstack([np.arange(0, n_vox)] * 3)[valid_fixs]
    fix_ind = np.kron(np.arange(0, 3), np.ones(n_vox)).astype('int')[valid_fixs]

    order = np.lexsort(np.c_[fix_ind, vox_ind].T)
    vox_ind = vox_ind[order]
    fix_ind = fix_ind[order]

    locations = coords[vox_ind]
    orientations = all_orientations[valid_fixs][order]
    nei = [neighbours(p, locations, o, orientations, angle_th=angle_threshold) for p, o in zip(locations, orientations)]
    edges = np.array(list(chain.from_iterable([[(n, q_) for q_ in q] for n, q in enumerate(nei)])))
    n_fix = orientations.shape[0]

    graph = csr_matrix((np.ones(edges.shape[0]), (edges[:, 0], edges[:, 1])), shape=(n_fix, n_fix))
    return graph, vox_ind, fix_ind, locations, orientations


def calc_tfce_towsided(graph, stat, H=2, E=0.5, step=0.1):
    """
    Calculate tfce given a graph and statistics (separately for positive and negative numbers)
    Args:
        graph: adjacency matrix in a sparse format (n_fix, n_fix)
        stat: statistics (e.g. zscore) (n_fix, )
        H: exponent for height
        E: exponent for cluster extent.
        step: step size for integration.
    """
    tfce = np.zeros_like(stat)

    pos_idx = stat > 0
    pos_graph = graph[pos_idx, :].tocsc()[:, pos_idx]
    tfce[pos_idx] = calc_tfce(pos_graph, stat[pos_idx], H, E, step)

    neg_idx = stat < 0
    neg_graph = graph[neg_idx, :].tocsc()[:, neg_idx]
    tfce[neg_idx] = calc_tfce(neg_graph, -stat[neg_idx], H, E, step)
    return tfce


def calc_tfce(graph, stat, H=2, E=0.5, step=0.1):
    """
    The main TFCE function. Calculates tfce only for positive stats.
    Args:
        graph: adjacency matrix in a sparse format (n_fix, n_fix)
        stat: statistics (e.g. zscore) (n_fix, )
        H: exponent for height
        E: exponent for cluster extent.
        step: step size for integration.
    Returns:
        TFCE
    """
    dh = np.arange(1e-3, stat.max(), step)
    tmp = np.zeros((graph.shape[0], len(dh)))
    for i, thr in enumerate(dh):
        g = graph[stat > thr, :].tocsc()[:, stat > thr]
        n_components, labels = connected_components(csgraph=g, directed=False, return_labels=True)
        c, _ = np.histogram(labels, bins=np.arange(-0.5, n_components + 0.5))
        clust_size = c[labels]
        tmp[stat > thr, i] = (clust_size ** E) * (thr ** H)

    return simpson(y=tmp, x=dh, axis=1)
