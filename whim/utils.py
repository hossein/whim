import sys
import numpy as np
import matplotlib.pyplot as plt
from . import diffusion_models as dm
from scipy.special import hyp1f1, gamma
import scipy.stats as st
import subprocess
import pkg_resources
import os.path as op
from file_tree.parse_tree import available_subtrees
from joblib import Parallel, delayed
import tqdm
from typing import Callable, Tuple
from itertools import permutations

np.seterr(all='raise')


def load():
    for filename in pkg_resources.resource_listdir(__name__, "trees"):
        with pkg_resources.resource_stream(__name__, op.join("trees", filename)) as f:
            available_subtrees[filename] = f.read().decode()


def run_parallel(func, n_iters, debug=False, print_progress=True, prefer=None):
    """
    runs a for loop in parallel.
    Args:
        func: callable function o = f(i)
        n_iters: number of iterations
        debug: set false to run normally (good for debugging)
        print_progress: prints the progress bar to output
        prefer: backend type. Any of {‘processes’, ‘threads’} or None
    """
    if debug is False:
        iters = range(n_iters)
        if print_progress is True:
            iters = tqdm.tqdm(iters, file=sys.stdout)

        res = Parallel(n_jobs=8, prefer=prefer)(delayed(func)(i) for i in iters)
    else:
        res = []
        for i in range(n_iters):
            res.append(func(i))

    if hasattr(res[0], '__len__'):
        return [np.stack([r[i] for r in res], axis=0) for i in range(len(res[0]))]
    else:
        return np.array(res)


class Orientation:
    def __init__(self, x=None, y=None, z=None, theta=None, phi=None):
        self._x = x
        self._y = y
        self._z = z

        self._theta = theta
        self._phi = phi

    @classmethod
    def from_angles(cls, phi, theta):
        return cls(phi=phi, theta=theta)

    @classmethod
    def from_dyad(cls, x, y, z):
        return cls(x=x, y=y, z=z)

    @property
    def x(self, ):
        if self._x is None:
            self.compute_dyads()
        return self._x


def log_inv_gamma(x, a, b):
    assert np.all(x > 0)
    return -(a+1) * np.log(x) - b/x


def watson_pdf(x, mu, odi):
    """
    computes normalized watson distribution.
    args:
        x: points on the unit sphere array (n, 3)
        mu : mean orientation vector(3,)
        odi : dispersion index scalar
    return:
        (n,) watson pdf
    """
    x = np.atleast_2d(x)
    x = x / np.linalg.norm(x, axis=1, keepdims=True)
    mu = mu / np.linalg.norm(mu)
    t = (x @ mu.T) ** 2
    k = 1 / np.tan(np.pi/2 * odi)

    return np.exp(t * k) / watson_norm(k)


def watson_pdf_angles_log(phi_x, th_x, phi_m, th_m, odi):
    """
    computes normalized watson distribution.
    args:

    return:
        (n,) watson pdf
    """
    cost = np.sin(th_x) * np.sin(th_m) * np.cos(phi_x - phi_m) + np.cos(th_x) * np.cos(th_m)
    k = 1 / np.tan(np.pi/2 * odi)
    return k * cost ** 2 - np.log(watson_norm(k))


def watson_norm(k):
    """
    computes normalizing constant for watson distribution.
    """
    return 4 * np.pi * hyp1f1(1/2, 3/2, k)


def watson_sample(mu=np.array([0, 0, 1]), k=0.01, n_samples=1000):
    """
    Samples from watson distribution using mcmc
    Args:
        mu: mean orientation (1, 3)
        k: concentration parameter
        n_samples: number of samples

    Return:
        xyz samples (n, 3)
    """
    f = lambda x: np.log(watson_pdf(p2c(*x), mu, k))
    phth_samples, probs = mcmc(f, args=(), p0=np.array([0, 0]), burnin=1000, n_samples=n_samples)
    return p2c(phth_samples[:, 0], phth_samples[:, 1])


def gaussian_pdf(x, mu, k):
    """
    gaussian distribution on the angle between orientations.
    """
    x = np.atleast_2d(x)
    x = x / np.linalg.norm(x, axis=1, keepdims=True)
    mu = mu / np.linalg.norm(mu)
    t = np.arcsin(np.sqrt(abs(1-(x @ mu.T) ** 2))) ** 2
    return np.exp(-t/(2*k))/gaussian_norm(k)


def gaussian_sample(mu=np.array([0, 0, 1]), k=0.1, n_samples=1000):
    """
    Samples from watson distribution using mcmc
    Args:
        mu: mean orientation
        k: concentration parameter
        n_samples: number of samples

    Return:
        xyz samples (n, 3)
    """
    f = lambda x: np.log(gaussian_pdf(p2c(*x), mu, k))
    phth_samples, probs = mcmc(f, args=(), p0=np.array([0, 0]), burnin=1000, n_samples=n_samples)
    return p2c(*phth_samples.T)


def gaussian_norm(k):
    return np.sqrt(2*np.pi*k) * (st.norm(scale=np.sqrt(k)).cdf(np.pi/2) - st.norm(scale=np.sqrt(k)).cdf(-np.pi/2))


def p2c(phi, theta, r=1):
    x = r * np.sin(theta) * np.cos(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(theta)
    return np.stack([x, y, z], axis=0).T


def p2c_fast(phi, theta, r=1):
    n = getattr(phi, '__len__', lambda: 1)()
    xyz = np.zeros((n, 3))
    rsin = r * np.sin(theta)
    xyz[:, 0] = rsin * np.cos(phi)
    xyz[:, 1] = rsin * np.sin(phi)
    xyz[:, 2] = r * np.cos(theta)
    return xyz


def c2p(xyz):
    """
        converts cartesian to spherical coordinates
    """
    x, y, z = xyz.T
    r = (x ** 2 + y ** 2 + z ** 2) ** 0.5
    if np.isscalar(r):
        if r == 0:
            r = 1
    else:
        r[r == 0] = 1  # origin is a singularity,

    theta = np.arccos(z/r)
    phi = np.arctan2(y, x)
    if np.isscalar(phi):
        if np.sin(theta) == 0:
            phi = 0
    else:
        phi[np.sin(theta) == 0] = 0

    return phi % (2 * np.pi), theta % np.pi


def c2p_saad(x, y, z):
    xyz = np.array((x, y, z), ndmin=2)
    n = np.linalg.norm(xyz, axis=1)
    n[n == 0] = 1
    xyz = xyz / n[:, np.newaxis]
    th = np.arccos(xyz[:, 2])
    st = np.sin(th)
    idx = (st == 0)
    st[idx] = 1
    ph = np.arctan2(xyz[:, 1]/st, xyz[:, 0]/st)
    ph[idx] = 0
    return ph, th, n


assert (p2c(0, 0) == np.array([0, 0, 1])).all()
assert c2p(np.array((0, 0, 1))) == (0, 0)


def ball_sticks_1(d, f_0, f_1, phi_1, theta_1, bvals, bvecs):
    """
    Takes the parameters of ball & stick model and acquistion parameters to generate diffusion signal.
    """
    assert 0 <= f_1 <= 1
    signal = f_0 * np.exp(-d * bvals) + f_1 * np.exp(-d * bvals * p2c(phi_1, theta_1)[np.newaxis, :].dot(bvecs.T) ** 2)
    return np.squeeze(signal)


def fibonacci_sphere(samples=1):
    """
    Creates N points uniformly-ish distributed on the sphere

    Args:
        samples : int
    """
    phi = np.pi * (3. - np.sqrt(5.))  # golden angle in radians

    i = np.arange(samples)
    y = 1 - 2. * (i / float(samples - 1))
    r = np.sqrt(1 - y * y)
    t = phi * i
    x = np.cos(t) * r
    z = np.sin(t) * r

    points = np.asarray([x, y, z]).T

    return points


def plot_sphere(r=1, alpha=0.5):
    fig = plt.figure(figsize=(4, 4))
    ax = fig.add_subplot(projection='3d')
    u, v = np.mgrid[0:2 * np.pi:50j, 0:np.pi:40j]
    x = r * np.cos(u) * np.sin(v)
    y = r * np.sin(u) * np.sin(v)
    z = r * np.cos(v)
    ax.plot_surface(x, y, z, color='lightgray', alpha=alpha)
    return ax


def dyadic_avg(phi, theta):
    """
        computes the dyadic average.
        * the average always points to positive side of x axis.
        Args:
              phi: array of phi values
              theta:  array of theta values
        return:
            average phi and theta
    """
    d = p2c(phi, theta)
    if np.isnan(d).any():
        return np.nan, np.nan
    else:
        d_avg = np.linalg.eigh(np.einsum('ik,ip->kp', d, d))[1][..., -1]
        phi_avg, theta_avg = c2p(d_avg)

    return phi_avg, theta_avg


def calc_dispersion(vecs):
    """
        computes dispersion for a bunch of normalized vectors.

        Args:
              vecs: array of vectors (n,3)
        return:
            dispersion
    """
    vecs = vecs / np.linalg.norm(vecs, axis=1)[:, np.newaxis]
    return 1 - np.linalg.eigh(np.einsum('ik,ip->kp', vecs, vecs)/vecs.shape[0])[0].max()


def average_samples(samples, angle_idx=[]):
    """
    take samples and computes the average, everything is ordinary averaging except for angles dyadic average is used.
    args:
        samples: (n, d)
        angle_idx : (k, 2) array, or nested list that contains index of phi and theta for each vector
    return:
        (1, d)
    """
    mean_samples = np.mean(samples, axis=0)
    if len(angle_idx) > 0:
        for (phi_idx, theta_idx) in np.atleast_2d(angle_idx):
            mean_samples[phi_idx], mean_samples[theta_idx] = \
                dyadic_avg(samples[:, phi_idx], samples[:, theta_idx])

    return mean_samples


def mcmc(posterior: Callable, args: Tuple, p0: np.ndarray, cov=None, bounds=None, step_size=1e0, n_samples=1000, burnin=100, skips=10):
    """
    MCMC sampling of a distribution
    Args:

        posterior: The log pdf of distribution (Callable function like f(p0, *args)
        args: parameters of the distribution (tuple)
        p0: initial point (array like)
        cov: covariance of samples
        bounds: sample bounds (n_params, 2)

    Returns:
        Samples, pos
    """
    p0 = np.atleast_1d(p0)
    if cov is None:
        cov = np.eye(len(p0))

    cov = np.atleast_2d(cov)
    min_eig = np.linalg.eigvalsh(cov)[0]
    correction = np.maximum(1e-14 - min_eig, 0)
    L1 = np.linalg.cholesky(cov + correction * np.eye(cov.shape[0])) / np.sqrt(len(p0))

    if bounds is None:
        bounds = np.array([[-np.inf, np.inf]] * len(p0))

    assert (p0 >= bounds[:, 0]).all() and (p0 <= bounds[:, 1]).all(), "P0 doesnt fit to the bounds"

    current = np.array(p0)
    prob_cur = posterior(current, *args)
    samples = []
    all_probs = []
    jumps = n_samples * skips + burnin
    for j in range(jumps):
        proposed = current + L1 @ np.random.randn(*current.shape) * step_size
        if (proposed >= bounds[:, 0]).all() and (proposed <= bounds[:, 1]).all():
            prob_next = posterior(proposed, *args)
            diff = np.clip(prob_next - prob_cur, -700, 700)  # to avoid overflow/underflow
            if np.exp(diff) > np.random.rand():
                current = proposed
                prob_cur = prob_next

        samples.append(current)
        all_probs.append(prob_cur)
    return np.squeeze(np.stack(samples, axis=0))[burnin::skips], np.array(all_probs)[burnin::skips]


def iterative_mcmc(posterior, args, p0, n_samples=1000, bounds=None, repeats=10, burnin=100, skips=10, starting_step_size=1e-3):
    """MCMC sampling with adjusting the covariance """
    p0 = np.array(p0)
    current_cov = np.eye(len(p0)) * starting_step_size ** 2
    start = np.array(p0)
    for _ in range(repeats):
        results, _ = mcmc(posterior, args, start, current_cov, bounds=bounds, step_size=1,
                          n_samples=n_samples // repeats + len(p0), burnin=0, skips=1)
        start = results[-1]
        current_cov = np.cov(results.T)

    return mcmc(posterior, args, start, current_cov, bounds=bounds, n_samples=n_samples,
                burnin=burnin, skips=skips, step_size=1)


def generate_acq(n_b0=10, n_dir=64, b=(1, 2, 3)):
    bvals = np.zeros(n_b0)
    bvecs = fibonacci_sphere(n_b0)

    for b_ in b:
        bvals = np.concatenate([bvals, np.ones(n_dir) * b_])
        bvecs = np.concatenate([bvecs, fibonacci_sphere(n_dir)])
    return bvals, bvecs


def grad(f, p, bounds, dp=1e-6):
    dp = np.maximum(1e-10, abs(p * dp))
    g = []
    for i in range(len(p)):
        pi = np.zeros(len(p))
        pi[i] = dp[i]

        u = p + pi
        if u[i] > bounds[i][1]:
            u[i] = bounds[i][1]

        l = p - pi
        if l[i] < bounds[i][0]:
            l[i] = bounds[i][0]

        g.append((f(u) - f(l)) / (u[i] - l[i]))
    return np.stack(g, axis=0)


def hessian(f, p, bounds, dp=1e-6):
    g = lambda p_: grad(f, p_, bounds, dp)
    return grad(g, p, bounds, dp)


def fit_tensor(data, bvals, bvecs):
    """
    fits tensor model to a single diffusion data
    Args:
        data: data array (n, )
        bvals: bvals array (n, )
        bvecs: bvecs  array(n,3)
    returns:
        s0 and the lower triangular elements of tensor matrix
    """
    data = np.abs(np.squeeze(data))
    bmat = -np.c_[bvecs[:, 0] ** 2, 2 * bvecs[:, 0] * bvecs[:, 1],
                  2 * bvecs[:, 0] * bvecs[:, 2], bvecs[:, 1] ** 2,
                  2 * bvecs[:, 2] * bvecs[:, 1], bvecs[:, 2] ** 2] * bvals[:, np.newaxis]
    bmat = np.c_[np.ones((len(bvals), 1)), bmat]
    beta = np.linalg.pinv(bmat) @ np.log(data)
    beta[0] = np.exp(beta[0])

    return beta


def fit_tensor_old(data, bvals, bvecs):
    """
    fits tensor model to a single diffusion data
    Args:
        data: data array (n, )
        bvals: bvals array (n, )
        bvecs: bvecs  array(n,3)
    returns:
        s0 and the lower triangular elements of tensor matrix
    """

    s0 = np.mean(data[bvals == 0])
    bmat = -np.c_[bvecs[:, 0] ** 2, 2 * bvecs[:, 0] * bvecs[:, 1],
                  2 * bvecs[:, 0] * bvecs[:, 2], bvecs[:, 1] ** 2,
                  2 * bvecs[:, 2] * bvecs[:, 1], bvecs[:, 2] ** 2] * bvals[:, np.newaxis]
    beta = np.linalg.pinv(bmat) @ (np.log(data) - np.log(s0))

    return np.append(s0, beta)


def dvec2mat(vec):
    return np.array([[vec[0], vec[1], vec[2]],
                     [vec[1], vec[3], vec[4]],
                     [vec[2], vec[4], vec[5]]])


def clip_angles(phi, theta):
    """
    clip theta and phi to the reference ranges;
    theta in (0, pi) and phi in (0, 2pi)
    """
    return np.mod(phi, 2 * np.pi), np.mod(theta, np.pi)


def align2posx(phi, theta):
    """
        flips the vector to point toward positive direction of x axis.
    """
    xyz = p2c(phi, theta)
    sgn_x = np.sign(xyz[:, 0])
    sgn_x[xyz[:, 0] == 0] = 1
    phi_r, theta_r = c2p(xyz * sgn_x)
    return phi_r, theta_r


def align2ref(phi, theta, phi_ref, theta_ref):
    """
    Alignes vector to direction of ref, if the angle between them is greater than pi/2, flips the vec
    Args:
        phi: (n,)
        theta: (n,)
        phi_ref: (n,)
        theta_ref: (n,)
    Return:
        aligned vecs
    """

    mu = p2c(phi, theta)
    mu_ref = p2c(phi_ref, theta_ref)
    sgn = np.sign((mu * mu_ref).sum(axis=1))[:, np.newaxis]
    sgn[sgn == 0] = 1
    mu *= sgn
    new_phi, new_theta = c2p(mu)

    return new_phi, new_theta


def align2avg(phi, theta):
    """
    Alignes vector to direction of dyadic average
    Args:
        phi: (n,)
        theta: (n, )
    Return:
        aligned phi and theta
    """
    nans = np.isnan(phi) | np.isnan(theta)
    phi_avg, theta_avg = dyadic_avg(phi[~nans], theta[~nans])
    p, t = align2ref(phi, theta, phi_avg * np.ones_like(phi), theta_avg * np.ones_like(phi))
    return p, t


def calc_fa(l):
    """calculate FA based on eigenvalues
    """
    lm = np.mean(l)
    return np.sqrt(3. / 2.) * np.sqrt(np.sum((l - lm) ** 2)) / np.sqrt(np.sum(l ** 2))


def dti_measures(dti_params):
    """
    extract dti measures from vector of parameters
    Args:
        dti_params: (n, 7) vector of free parameters [s0, dxx, dxy, dxz, dyy, dyz, dzz]
    return:
    """
    s0, d_vec = dti_params[0], dti_params[1:]
    d_mat = dvec2mat(d_vec)
    md = np.trace(d_mat) / 3
    l, v = np.linalg.eig(d_mat)
    idx = np.argsort(l)[::-1]
    l, v = l[idx], v[:, idx]
    fa = calc_fa(l)
    phi, theta = c2p(v.T)
    return s0, md, fa, l[0], l[1], l[2], phi, theta


def dti2bs(bvals, bvecs, n_sticks=1, training_params=None, n_samples=1000):
    """"
    Cumputes a linear mapping from dti parameters to ball and stick parameters
    Args:
         bvals:
         bvecs:
         n_sticks:
         training_params:
         n_samples:
    """
    # generate bas data:
    bs_model = dm.BallStick(n_sticks=n_sticks, bvals=bvals, bvecs=bvecs)
    if training_params is None:
        bs_params = bs_model.gen_samples(n_samples)
    else:
        bs_params = training_params.copy()

    data = np.stack([bs_model.calc_signal_dict(p) for p in bs_params], axis=0)

    # Fit tensor
    dti_params = np.stack([fit_tensor(d, bvals, bvecs) for d in data], axis=0)
    dti_meas = [dti_measures(d) for d in dti_params]

    # calc_signal_dict the mapping
    desmat = np.stack([d[1:-2] for d in dti_meas], axis=0)  # exclude orientations
    s0_beta = np.matmul(np.linalg.pinv(desmat), bs_params[:, 1])
    d_beta = np.matmul(np.linalg.pinv(desmat), bs_params[:, 0])
    fs = np.stack([bs_params[:, i] for i, p in enumerate(bs_model.param_names) if 'f_' in p], axis=1)
    f_beta = np.matmul(np.linalg.pinv(desmat), fs)

    # below is the mapping function
    def mapping(dti_p):
        dti_meas = dti_measures(dti_p)
        p = np.asarray(dti_meas[1:-2]).T
        s0 = dti_meas[0] ## np.matmul(p, s0_beta)
        d = np.matmul(p, d_beta)
        fs = np.matmul(p, f_beta)
        ph, th = dti_meas[-2:]
        params = [[d, s0]]
        params.extend([[fs[i], ph[i], th[i]] for i in range(n_sticks)])
        params = [p for comp in params for p in comp]
        return params

    return mapping, [d_beta, s0_beta, f_beta]


def run_command(cmd, time_out=None):
    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT,
                                         shell=True, timeout=time_out, universal_newlines=True)
    except subprocess.CalledProcessError as exc:
        print("Status : FAIL", exc.returncode, exc.output)
    else:
        print("Output: \n{}\n".format(output))


def rotate(vec, rot):
    """
    Rotates the vectors with the rotation matrices.
    Args:
        vec: (n, 3)
        rot: (n, 3, 3)
    Returns:
        normed(n, 3)
    """
    if vec.ndim == 1:
        vec = vec[np.newaxis, :]
    if rot.ndim == 2:
        rot = rot[np.newaxis, :, :]

    vecrot = np.einsum('ikj, ij->ik', rot, vec)

    length = np.linalg.norm(vecrot, axis=1)
    idx = length > 0
    normed = np.zeros_like(vec)
    normed[idx] = vecrot[idx] / (length[idx][:, np.newaxis])
    return normed


def generate_rotmat(a_x, a_y, a_z):
    """
    Create rotation matrix that rotates a_x around x, a_y around y, a_z around z axis:
    Args:
        a_x:
        a_y:
        a_z:
    Returns:
        (3x3) rotation matrix
    """

    r_x = np.array([[1, 0, 0], [0, np.cos(a_x), -np.sin(a_x)], [0, np.sin(a_x), np.cos(a_x)]])
    r_y = np.array([[np.cos(a_y), 0, np.sin(a_y)], [0, 1, 0], [-np.sin(a_y), 0, np.cos(a_y)]])
    r_z = np.array([[np.cos(a_z), -np.sin(a_z), 0], [np.sin(a_z), np.cos(a_z), 0], [0, 0, 1]])

    return r_z @ r_y @ r_x


def rot_to_angle(rotmat):
    """
    computes the axis and the euler angle of rotation
    Args:
        rotmat: 3x3 rotation matrix
    Return:
        ax: (3,) Axis of rotation
        angle: the amount of rotation
    """
    s, v, d = np.linalg.svd(rotmat)  # remove shearing
    x = s @ d
    vals, vecs = np.linalg.eig(x)
    ax = np.squeeze(np.real(vecs[:, np.argwhere(np.imag(vals) == 0)]))  # eigen vector with the real eigen value.
    angle = np.arccos((np.trace(x) - 1)/2)
    return ax, angle


log2pi = np.log(2 * np.pi)


def log_mvnpdf(x, mean, cov):
    """
    log of multivariate normal distribution. identical output to scipy.stats.multivariate_normal(mean, cov).logpdf(x) but faster.
    :param x: input numpy array (n, d)
    :param mean: mean of the distribution numpy array same size of x (n, d)
    :param cov: covariance of distribution, numpy array or scalar (n, d, d)
    :return scalar
    """
    cov = np.atleast_2d(cov)
    d = mean.shape[-1]
    offset = np.atleast_2d(x - mean)
    if cov.ndim == 2:
        cov = cov[np.newaxis, :, :]

    expo = -0.5 * np.einsum('ij,ijk,ik->i', offset, np.linalg.inv(cov), offset)
    # expo = -0.5 * (x - mean) @ np.linalg.inv(cov) @ (x - mean).T
    nc = -0.5 * (log2pi * d + np.linalg.slogdet(cov)[1])  #  we expect the covariance to be PD

    # var2 = np.log(multivariate_normal(mean=mean, cov=cov).pdf(x))
    return expo + nc


def norm_log_pdf(x, mean, sigma2):
    """
    expects all inputs being vectors of the same size
    """
    expo = -0.5 * ((x - mean) ** 2) / sigma2
    nc = -0.5 * (log2pi + np.log(sigma2))
    return expo + nc


def glm(data, des_mat, con):
    """
    Runs glm on a dataset given design matrix and constrasts.
    Calculates parameter estimates and copes
    Args:
        data: (n_subj, n_vox) array
        des_mat: (n_subj x n_evs) array
        con : (n_con, n_evs) array

    Return:
        beta: (n_evs, n_vox)
        copes: (m, n_vox)
        varcopes: (m, n_vox)

    data = x @ beta + E
    """
    beta = np.linalg.pinv(des_mat) @ data
    copes = con @ beta

    r = data - des_mat @ beta
    sigma_sq = np.array([np.cov(a) for a in r.T])
    varcopes = (sigma_sq[..., np.newaxis] * np.diagonal(con @ np.linalg.inv(des_mat.T @ des_mat) @ con.T)).T
    z_score = copes / varcopes
    return beta, copes, varcopes, z_score


def average_orientations(orients):
    return np.linalg.eigh(np.einsum('ik,ip->kp', orients, orients))[1][..., -1]


def relable_bruteforce(template, subj_orientations):
    """
    This function gets an initial template and a list of subject orientations and return a labeling of
    subject orientations.
    Args:
        template: k x 3 orientation of the template fibres.
        subj_orientations: n x k x 3 subject orientations
    Returns:
        labeling: n x k : matrix containing the labels of subject fibre orientations.
    """
    n_subj, n_fibres = subj_orientations.shape[:2]
    labelings = np.zeros((n_subj, n_fibres), dtype=int)
    all_perms = list(permutations(range(n_fibres)))
    costs = np.zeros(len(all_perms))
    for subj, orients in enumerate(subj_orientations):
        dists = 1 - np.abs(orients @ template.T)
        for k, p in enumerate(all_perms):
            costs[k] = np.sum([dists[i, p[i]] for i in range(n_fibres)])
        best = np.argmin(costs)
        labelings[subj] = all_perms[best]
    return labelings


def my_kmeans_noempty(X, k, max_iter=1000):
    """
    args:
        X: (n, d) array
        k: number of clusters
        max_iter: maximum number of iterations
    Return:
        cluster labels, cluster centers
    """
    assert k <= X.shape[0]
    if k <= 1:
        return np.zeros(X.shape[0]), X.mean(axis=0)

    iters = 0
    change = 1
    centers = X[np.random.randint(0, X.shape[0], k)]

    while iters < max_iter and change > 0:
        old_centers = centers.copy()
        dists = 1 - np.abs(X @ centers.T)
        labels = np.argmin(dists, axis=1)

        if len(np.unique(labels)) < k:  # empty cluster
            furthest_points = np.argmax(dists, axis=0)  # find the furthest point in each cluster
            furthest_dists = np.max(dists, axis=0)  # find the distances of those points from all centres
            furthest_idx = np.argsort(furthest_dists)  # sort them
            for j in range(k):  # fill the empty cluster with the furthest points of others.
                if (labels == j).sum() == 0:
                    labels[furthest_points[furthest_idx[-1]]] = j
                    furthest_idx = furthest_idx[:-1]

        for j in range(k):  # update the centers
            centers[j] = average_orientations(X[labels == j])
            centers[j] = centers[j] / np.linalg.norm(centers[j])

        change = np.linalg.norm(centers - old_centers)
        iters += 1

    return labels.astype(int), centers


def peak_matching(orients, max_iters=10):
    """
    Aligns the lable of fibre population across subjects
    Args:
        orients: matrix of individual subject orientations of size (n_subj, n_fibres, 3)
        max_iters: maximum number of iterations
    """

    n_subj, n_fibres = orients.shape[:2]
    cluster_labels, cluster_centres = my_kmeans_noempty(orients.reshape(-1, 3), k=n_fibres)

    for i in range(max_iters):
        new_labels = relable_bruteforce(cluster_centres, orients)
        for j in range(n_fibres):
            new_cluster = np.vstack([orients[s, l == j] for s, l in enumerate(new_labels)])
            cluster_centres[j] = average_orientations(new_cluster)
    return new_labels, cluster_centres
