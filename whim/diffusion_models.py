# Thi module contains implementation of the biophysical models for diffusion.
import numpy as np
import scipy.stats as st
from whim import utils
from typing import Mapping


class BallStick:
    def __init__(self, n_sticks, bvals=None, bvecs=None):
        self.param_names = ['s_0', 'd'] + [f'{p}_{i + 1}' for i in range(n_sticks) for p in ['f', 'phi', 'theta']]
        self.param_file_names = ['s_0', 'd'] + [f'{p}_{i + 1}' for i in range(n_sticks) for p in ['f', 'dyads']]

        self.n_sticks = n_sticks
        self.angle_idx = np.array([[self.param_names.index(f'{p}_{i}')
                                    for i in range(1, n_sticks + 1) if f'{p}_{i}' in self.param_names]
                                   for p in ['phi', 'theta']]).T
        self.non_angle = np.array([i not in self.angle_idx.flat for i in range(len(self.param_names))])

        self.priors = {'s_0': st.uniform(loc=0.0, scale=1e6),
                       'd': st.truncnorm(loc=1.7, scale=0.3, a=-1.7 / 0.3, b=np.inf),
                       }

        self.priors.update({f'{p}_{i + 1}': v for i in range(n_sticks)
                            for p, v in zip(['f', 'phi', 'theta'], [st.uniform(loc=0.0, scale=1),
                                                                    st.uniform(loc=-2 * np.pi, scale=4 * np.pi),
                                                                    st.uniform(loc=-np.pi, scale=2 * np.pi)])})

        self.n_params = len(self.param_names)
        self.param_bounds = self._param_bounds()
        self.constraints = self._constraints()

        if (bvals is not None) and (bvecs is not None):
            self.set_acq(bvals, bvecs)
        else:
            self.bvals, self.bvecs, self.bvecs_phi, self.bvecs_th, self.bvecs_th_sin, self.bvecs_th_cos = [None] * 6

        self.ff_fast = [self.bas1, self.bas2, self.bas3][self.n_sticks - 1]

    def set_acq(self, bvals, bvecs):
        self.bvals = bvals
        self.bvecs = bvecs
        self.bvecs_phi, self.bvecs_th = utils.c2p(bvecs)
        self.bvecs_th_sin = np.sin(self.bvecs_th)
        self.bvecs_th_cos = np.cos(self.bvecs_th)

    def forward_model(self, params):
        return self.ff_fast(*params)

    def calc_signal_dict(self, params):
        """
        gets params as a dictionary and computes the signal.
        """
        if not isinstance(params, Mapping):
            assert len(self.param_names) == len(params)
            params = {k: v for k, v in zip(self.param_names, params)}

        f_sum = np.sum([params[f'f_{i + 1}'] for i in range(self.n_sticks)])
        # assert f_sum <= 1, "sum of signal fractions are greater than 1"

        f_0 = 1 - f_sum
        s = f_0 * np.exp(-params['d'] * self.bvals)

        for i in range(1, self.n_sticks + 1):
            cost = self.calc_cos_fast(params[f'phi_{i}'], params[f'theta_{i}'])
            s += np.squeeze(params[f'f_{i}'] * np.exp(-params['d'] * self.bvals * cost ** 2))
        return s * params['s_0']

    def calc_signal(self, params_vec):
        """
        calculate diffusion signals given vector or parameters
        """
        assert len(self.param_names) == len(params_vec)
        params = {k: v for k, v in zip(self.param_names, params_vec)}
        return self.calc_signal_dict(params)

    def _constraints(self):
        """
        Generate linear constraints for optimization
        return A, b matrices such that lb < Axparams < ub
        """

        f_idx = [i for i, p in enumerate(self.param_names) if 'f_' in p]
        # 0 < stick volume fraction sum  <1
        a = np.zeros(len(self.param_names))
        a[f_idx] = 1
        return a, 0, 0.99

    def _param_bounds(self):
        """
        returns parameter bounds for optimization:

        """
        bounds = np.zeros((self.n_params, 2))
        bounds[:, 0] = -np.inf  # for angles
        bounds[:, 1] = np.inf
        bounds[0] = 0, np.inf  # s0
        bounds[1] = 0, 10  # diffusivity
        f_idx = [i for i, p in enumerate(self.param_names) if 'f_' in p]
        bounds[f_idx, 0] = 0
        bounds[f_idx, 1] = 1
        return bounds

    def gen_samples(self, n_samples=1):
        params = {k: p.rvs(n_samples) for k, p in self.priors.items()}
        params['s_0'] = np.random.uniform(1000, 1000, n_samples)  # prior of s0 is meaningless.
        f0 = np.random.rand(n_samples)
        f_sum = np.sum([params[f'f_{i + 1}'] for i in range(self.n_sticks)], axis=0) + f0
        for i in range(self.n_sticks):
            params[f'f_{i + 1}'] /= f_sum
        return np.stack(list(params.values()), axis=1)

    def x0(self):
        """
        give initial values for optimization.
        """
        x = [1, 1]  # s0, diffusivity
        phi, th = utils.c2p(utils.fibonacci_sphere(2 * self.n_sticks))
        for i in range(self.n_sticks):
            x += [1 / (self.n_sticks + 1), phi[i], th[i]]

        return np.array(x)

    def derivatives(self, params):
        """
        returns the derivative with respect to each parameter.
        """

        if not isinstance(params, Mapping):
            assert len(self.param_names) == len(params)
            params = {k: v for k, v in zip(self.param_names, params)}

        f_sum = np.sum([params[f'f_{i + 1}'] for i in range(self.n_sticks)])
        # assert f_sum <= 1, "sum of signal fractions are greater than 1"

        f_0 = 1 - f_sum
        fi = np.array([f_0] + [params[f'f_{i + 1}'] for i in range(self.n_sticks)])
        cos_t = np.array([np.ones(len(self.bvecs))] + [utils.p2c(params[f'phi_{i + 1}'], params[f'theta_{i + 1}']).dot(self.bvecs.T) for i in range(self.n_sticks)])
        exps = np.array([-self.bvals * params['d'] * a ** 2 for a in cos_t])
        ai = np.exp(exps)

        derivative = np.zeros((self.n_params, len(self.bvals)))
        derivative[0] = (fi[np.newaxis, :] @ ai)
        derivative[1] = -params['s_0'] * self.bvals * (fi[np.newaxis, :] @ (cos_t ** 2 * ai)).sum(axis=0)
        for i in range(1, self.n_sticks + 1):
            derivative[i * 3 - 1] = params['s_0'] * (ai[i] - ai[0])
            c = -2 * params['s_0'] * params['d'] * self.bvals * (fi[i] * cos_t[i] * ai[i])
            derivative[i * 3] = c * np.sin(params[f'theta_{i}']) * \
                                (-self.bvecs[:, 0] * np.sin(params[f'phi_{i}']) +
                                 self.bvecs[:, 1] * np.cos(params[f'phi_{i}']))

            derivative[i * 3 + 1] = c * (np.cos(params[f'theta_{i}']) *
                                         (self.bvecs[:, 0] * np.cos(params[f'phi_{i}']) +
                                          self.bvecs[:, 1] * np.sin(params[f'phi_{i}'])) -
                                         self.bvecs[:, 2] * np.sin(params[f'theta_{i}']))
        return derivative

    def calc_cos_fast(self, phi, th):
        return self.bvecs_th_sin * np.sin(th) * np.cos(self.bvecs_phi - phi) + self.bvecs_th_cos * np.cos(th)

    def bas1(self, s0, d, f1, phi1, th1):
        cost = self.calc_cos_fast(phi1, th1)
        return s0 * ((1 - f1) * np.exp(-d * self.bvals)
                     + f1 * np.exp(-d * self.bvals * cost ** 2))

    def bas2(self, s0, d, f1, phi1, th1, f2, phi2, th2):
        cost1 = self.calc_cos_fast(phi1, th1)
        cost2 = self.calc_cos_fast(phi2, th2)
        return s0 * ((1 - f1 - f2) * np.exp(-d * self.bvals)
                     + f1 * np.exp(-d * self.bvals * cost1 ** 2)
                     + f2 * np.exp(-d * self.bvals * cost2 ** 2))

    def bas3(self, s0, d, f1, phi1, th1, f2, phi2, th2, f3, phi3, th3):
        cost1 = self.calc_cos_fast(phi1, th1)
        cost2 = self.calc_cos_fast(phi2, th2)
        cost3 = self.calc_cos_fast(phi3, th3)
        return s0 * ((1 - f1 - f2 - f3) * np.exp(-d * self.bvals)
                     + f1 * np.exp(-d * self.bvals * cost1 ** 2)
                     + f2 * np.exp(-d * self.bvals * cost2 ** 2)
                     + f3 * np.exp(-d * self.bvals * cost3 ** 2))


class BallStick_DISO:
    def __init__(self, n_sticks, bvals, bvecs):
        self.param_names = ['s_0', 'd_iso', 'd_stick'] + \
                           [f'{p}_{i + 1}' for i in range(n_sticks) for p in ['f', 'phi', 'theta']]
        self.bvals = bvals
        self.bvecs = bvecs
        self.n_sticks = n_sticks
        self.angle_idx = np.array([[self.param_names.index(f'{p}_{i}')
                                    for i in range(1, n_sticks + 1) if f'{p}_{i}' in self.param_names]
                                   for p in ['phi', 'theta']]).T
        self.non_angle = np.array([i not in self.angle_idx.flat for i in range(len(self.param_names))])

        self.priors = {'s_0': st.uniform(loc=0.0, scale=1e6),
                       'd_iso': st.truncnorm(loc=1.7, scale=0.3, a=-1.7 / 0.3, b=np.inf),
                       'd_stick': st.truncnorm(loc=1.7, scale=0.3, a=-1.7 / 0.3, b=np.inf),
                       }

        self.priors.update({f'{p}_{i + 1}': v for i in range(n_sticks)
                            for p, v in zip(['f', 'phi', 'theta'], [st.uniform(loc=0.0, scale=1),
                                                                    st.uniform(loc=-2 * np.pi, scale=4 * np.pi),
                                                                    st.uniform(loc=-np.pi, scale=2 * np.pi)])})

        self.n_params = len(self.param_names)
        self.param_bounds = self._param_bounds()
        self.constraints = self._constraints()

        self.bvecs_phi, self.bvecs_th = utils.c2p(bvecs)
        self.bvecs_th_sin = np.sin(self.bvecs_th)
        self.bvecs_th_cos = np.cos(self.bvecs_th)

        self.ff_fast = [self.bas1, self.bas2, self.bas3][self.n_sticks - 1]

    def forward_model(self, params):
        return self.ff_fast(*params)

    def calc_signal_dict(self, params):
        """
        gets params as a dictionary and computes the signal.
        """
        if not isinstance(params, Mapping):
            assert len(self.param_names) == len(params)
            params = {k: v for k, v in zip(self.param_names, params)}

        f_sum = np.sum([params[f'f_{i + 1}'] for i in range(self.n_sticks)])
        # assert f_sum <= 1, "sum of signal fractions are greater than 1"

        f_0 = 1 - f_sum
        s = f_0 * np.exp(-params['d_iso'] * self.bvals)

        for i in range(1, self.n_sticks + 1):
            cost = self.calc_cos_fast(params[f'phi_{i}'], params[f'theta_{i}'])
            s += np.squeeze(params[f'f_{i}'] * np.exp(-params['d_stick'] * self.bvals * cost ** 2))

        return s * params['s_0']

    def calc_signal(self, params_vec):
        """
        calculate diffusion signals given vector or parameters
        """
        assert len(self.param_names) == len(params_vec)
        params = {k: v for k, v in zip(self.param_names, params_vec)}
        return self.calc_signal_dict(params)

    def _constraints(self):
        """
        Generate linear constraints for optimization
        return A, b matrices such that lb < Axparams < ub
        """

        f_idx = [i for i, p in enumerate(self.param_names) if 'f_' in p]
        # 0 < stick volume fraction sum  <1
        a = np.zeros(len(self.param_names))
        a[f_idx] = 1
        return a, 0, 0.99

    def _param_bounds(self):
        """
        returns parameter bounds for optimization:

        """
        bounds = np.zeros((self.n_params, 2))
        bounds[:, 0] = -np.inf  # for angles
        bounds[:, 1] = np.inf
        bounds[0] = 0, np.inf  # s0
        bounds[1] = 0, 10  # iso diffusivity
        bounds[2] = 0, 10  # stick diffusivity
        f_idx = [i for i, p in enumerate(self.param_names) if 'f_' in p]
        bounds[f_idx, 0] = 0
        bounds[f_idx, 1] = 1
        return bounds

    def gen_samples(self, n_samples=1):
        params = {k: p.rvs(n_samples) for k, p in self.priors.items()}
        params['s_0'] = np.random.uniform(1, 1, n_samples)  # prior of s0 is too wide for generating samples.
        f0 = np.random.rand(n_samples)
        f_sum = np.sum([params[f'f_{i + 1}'] for i in range(self.n_sticks)], axis=0) + f0
        for i in range(self.n_sticks):
            params[f'f_{i + 1}'] /= f_sum
        return np.stack(list(params.values()), axis=1)

    def x0(self):
        """
        give initial values for optimization.
        """
        x = [1, 2, 1]  # diffusivities, s0
        phi, th = utils.c2p(utils.fibonacci_sphere(2 * self.n_sticks))
        for i in range(self.n_sticks):
            x += [1 / (self.n_sticks + 1), phi[i], th[i]]

        return np.array(x)

    def derivatives(self, params):
        """
        returns the derivative with respect to each parameter.
        """

        if not isinstance(params, Mapping):
            assert len(self.param_names) == len(params)
            params = {k: v for k, v in zip(self.param_names, params)}

        f_sum = np.sum([params[f'f_{i + 1}'] for i in range(self.n_sticks)])
        # assert f_sum <= 1, "sum of signal fractions are greater than 1"

        f_0 = 1 - f_sum
        fi = np.array([params[f'f_{i + 1}'] for i in range(self.n_sticks)])
        cos_t = np.array([utils.p2c(params[f'phi_{i + 1}'], params[f'theta_{i + 1}']).dot(self.bvecs.T)
                          for i in range(self.n_sticks)])
        exps = np.array([-self.bvals * params['d_stick'] * a ** 2 for a in cos_t])
        ai = np.exp(exps)
        a0 = np.exp(-self.bvals * params['d_iso'])

        derivative = np.zeros((self.n_params, len(self.bvals)))
        derivative[0] = (fi[np.newaxis, :] @ ai + a0 * f_0)  # s0
        derivative[1] = -params['s_0'] * f_0 * self.bvals * a0
        derivative[2] = -params['s_0'] * self.bvals * (fi[np.newaxis, :] @ (cos_t ** 2 * ai)).sum(axis=0)

        for i in range(self.n_sticks):
            derivative[3 + i * 3] = params['s_0'] * (ai[i] - a0)
            c = -2 * params['s_0'] * params['d_stick'] * self.bvals * (fi[i] * cos_t[i] * ai[i])
            derivative[4 + i * 3] = c * np.sin(params[f'theta_{i + 1}']) * \
                                    (-self.bvecs[:, 0] * np.sin(params[f'phi_{i + 1}']) +
                                     self.bvecs[:, 1] * np.cos(params[f'phi_{i + 1}']))

            derivative[5 + i * 3] = c * (np.cos(params[f'theta_{i + 1}']) *
                                         (self.bvecs[:, 0] * np.cos(params[f'phi_{i + 1}']) +
                                          self.bvecs[:, 1] * np.sin(params[f'phi_{i + 1}'])) -
                                         self.bvecs[:, 2] * np.sin(params[f'theta_{i + 1}']))
        return derivative

    def calc_cos_fast(self, phi, th):
        return self.bvecs_th_sin * np.sin(th) * np.cos(self.bvecs_phi - phi) + self.bvecs_th_cos * np.cos(th)

    def bas1(self, s0, d_iso, d_stick, f1, phi1, th1):
        cost = self.calc_cos_fast(phi1, th1)
        return s0 * ((1 - f1) * np.exp(-d_iso * self.bvals)
                     + f1 * np.exp(-d_stick * self.bvals * cost ** 2))

    def bas2(self, s0, d_iso, d_stick, f1, phi1, th1, f2, phi2, th2):
        cost1 = self.calc_cos_fast(phi1, th1)
        cost2 = self.calc_cos_fast(phi2, th2)
        return s0 * ((1 - f1 - f2) * np.exp(-d_iso * self.bvals)
                     + f1 * np.exp(-d_stick * self.bvals * cost1 ** 2)
                     + f2 * np.exp(-d_stick * self.bvals * cost2 ** 2))

    def bas3(self, s0, d_iso, d_stick, f1, phi1, th1, f2, phi2, th2, f3, phi3, th3):
        cost1 = self.calc_cos_fast(phi1, th1)
        cost2 = self.calc_cos_fast(phi2, th2)
        cost3 = self.calc_cos_fast(phi3, th3)
        return s0 * ((1 - f1 - f2 - f3) * np.exp(-d_iso * self.bvals)
                     + f1 * np.exp(-d_stick * self.bvals * cost1 ** 2)
                     + f2 * np.exp(-d_stick * self.bvals * cost2 ** 2)
                     + f3 * np.exp(-d_stick * self.bvals * cost3 ** 2))


class DTI:
    def __init__(self, bvals, bvecs):
        self.param_names = ['s_0'] + [f'd_{i}' for i in ['x', 'xy', 'xz', 'y', 'yz', 'z']]
        self.bvals = bvals
        self.bvecs = bvecs
        self.priors = {'s_0': st.uniform(loc=0.0, scale=1000)}
        self.priors.update({f'd_{i}': st.uniform(loc=0, scale=3) for i in ['x', 'xy', 'xz', 'y', 'yz', 'z']})

    def compute(self, params):
        """
        gets params as a dictionary and computes the signal
        """
        d_mat = np.array([[params['d_x'], params['d_xy'], params['d_xz']],
                          [params['d_xy'], params['d_y'], params['d_yz']],
                          [params['d_xz'], params['d_yz'], params['d_z']]])

        s = params['s_0'] * np.exp(-self.bvals * ((self.bvecs @ d_mat) * self.bvecs).sum(axis=1))
        return s

    def calc_signal(self, params_vec):
        assert len(self.param_names) == len(params_vec)
        params = {k: v for k, v in zip(self.param_names, params_vec)}
        return self.compute(params)


class BallStick_PerpendicularDiffusivity:
    def __init__(self, n_sticks, bvals, bvecs):
        self.param_names = ['s_0', 'd_iso', 'd_a'] + \
                           [f'{p}_{i + 1}' for i in range(n_sticks) for p in ['f', 'd_per', 'phi', 'theta']]
        self.bvals = bvals
        self.bvecs = bvecs
        self.n_sticks = n_sticks
        self.angle_idx = np.array([[self.param_names.index(f'{p}_{i}')
                                    for i in range(1, n_sticks + 1) if f'{p}_{i}' in self.param_names]
                                   for p in ['phi', 'theta']]).T
        self.non_angle = np.array([i not in self.angle_idx.flat for i in range(len(self.param_names))])

        self.priors = {'s_0': st.uniform(loc=0.75, scale=0.5),
                       'd_iso': st.truncnorm(loc=1.7, scale=0.3, a=-1.7 / 0.3, b=np.inf),
                       'd_a': st.truncnorm(loc=1.7, scale=0.3, a=-1.7 / 0.3, b=np.inf)
                       }

        self.priors.update({f'{p}_{i + 1}': v for i in range(n_sticks)
                            for p, v in zip(['f', 'd_per', 'phi', 'theta'],
                                            [st.uniform(loc=0.0, scale=1),
                                             st.truncnorm(loc=1.7, scale=0.3, a=-1.7 / 0.3, b=np.inf),
                                             st.uniform(loc=-2 * np.pi, scale=4 * np.pi),
                                             st.uniform(loc=-np.pi, scale=2 * np.pi)])})

        self.n_params = len(self.param_names)
        self.param_bounds = self._param_bounds()
        self.constraints = self._constraints()

        self.bvecs_phi, self.bvecs_th = utils.c2p(bvecs)
        self.bvecs_th_sin = np.sin(self.bvecs_th)
        self.bvecs_th_cos = np.cos(self.bvecs_th)
        self.derivatives = None

    def forward_model(self, params):
        return self.calc_signal(params)

    def calc_signal_dict(self, params):
        """
        gets params as a dictionary and computes the signal.
        """
        if not isinstance(params, Mapping):
            assert len(self.param_names) == len(params)
            params = {k: v for k, v in zip(self.param_names, params)}

        f_sum = np.sum([params[f'f_{i + 1}'] for i in range(self.n_sticks)])
        # assert f_sum <= 1, "sum of signal fractions are greater than 1"

        f_0 = 1 - f_sum
        s = f_0 * np.exp(-params['d_iso'] * self.bvals)

        for i in range(1, self.n_sticks + 1):
            cost = self.calc_cos_fast(params[f'phi_{i}'], params[f'theta_{i}'])
            s += np.squeeze(params[f'f_{i}'] * np.exp(
                -(self.bvals * (params[f'd_a'] - params[f'd_per_{i}']) * cost ** 2 + params[f'd_per_{i}'])))

        return s * params['s_0']

    def calc_signal(self, params_vec):
        """
        calculate diffusion signals given vector or parameters
        """
        assert len(self.param_names) == len(params_vec)
        params = {k: v for k, v in zip(self.param_names, params_vec)}
        return self.calc_signal_dict(params)

    def _constraints(self):
        """
        Generate linear constraints for optimization
        return A, b matrices such that lb < Axparams < ub
        """

        f_idx = [i for i, p in enumerate(self.param_names) if 'f_' in p]
        # 0 < stick volume fraction sum  <1
        a = np.zeros(len(self.param_names))
        a[f_idx] = 1
        return a, 0, 0.99

    def _param_bounds(self):
        """
        returns parameter bounds for optimization:

        """
        bounds = np.zeros((self.n_params, 2))
        bounds[:, 0] = -np.inf  # for angles
        bounds[:, 1] = np.inf
        bounds[0] = 0, np.inf  # s0
        bounds[1] = 0, 10  # iso diffusivity
        bounds[2] = 0, 10  # parallel diffusivity
        f_idx = [i for i, p in enumerate(self.param_names) if 'f_' in p]
        bounds[f_idx, 0] = 0
        bounds[f_idx, 1] = 1

        d_idx = [i for i, p in enumerate(self.param_names) if 'd_per_' in p]
        bounds[d_idx, 0] = 0
        bounds[d_idx, 1] = 3

        return bounds

    def calc_cos_fast(self, phi, th):
        return self.bvecs_th_sin * np.sin(th) * np.cos(self.bvecs_phi - phi) + self.bvecs_th_cos * np.cos(th)

    def x0(self):
        """
        give initial values for optimization.
        """
        x = [1, 3, 2]  # s0, d_iso, d_a
        phi, th = utils.c2p(utils.fibonacci_sphere(2 * self.n_sticks))
        for i in range(self.n_sticks):
            x += [1 / (self.n_sticks + 1), 1, phi[i], th[i]]

        return np.array(x)