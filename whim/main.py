# this module creates the commandline interface.
import glob
import sys
import warnings
from whim import hbm, image_io, diffusion_models, utils
import numpy as np
import argparse
import importlib.resources
from fsl_pipe import Pipeline, In, Out, Ref
from typing import List, Tuple
from file_tree import FileTree
from fsl_pipe.job import RunMethod
import os
import logging
from timeit import default_timer as timer
from datetime import timedelta


def main(argv=None):
    """
    The main wrapper function to parse the input from commandline and run the requested pipeline.
    """
    args = parse_args(argv)
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', stream=sys.stdout)

    if args.func == 'subject':
        fit_subject(bvals=args.bvals, bvecs=args.bvecs, dmri=args.data,
                    mask=args.mask, diff2std=args.xfm, output_dir=args.output,
                    n_sticks=args.nf, prior=args.prior, init=args.init, model=args.model, includeall=args.includeall,
                    a_n=args.alpha, b_n=args.beta, jac=args.jac)

    elif args.func == 'group':
        estimate_group(subject_dirs=args.subjdirs, mask=args.mask, output_dir=args.output,
                       a_g=float(args.alpha), b_g=float(args.beta), nf=args.nf)

    elif args.func == 'EM':
        expectation_maximisation(file_tree=args.file_tree, output=args.output,
                                 mask=args.mask, model=args.model, nf=args.nf, max_iters=args.iters,
                                 jac=args.jac, pipeline_method=args.pipeline_method, maxjobtime=int(args.maxjobtime),
                                 a_g=args.groupalpha, b_g=args.groupbeta, a_n=args.noisealpha, b_n=args.noisebeta)
    elif args.func == 'template':
        make_template(modeldirs=args.modeldirs, criterion=args.criterion,
                      mask=args.mask, thresholds=args.thresh, output=args.output)

    elif args.func == 'fixel':
        fit_fixel(bvals=args.bvals, bvecs=args.bvecs, dmri=args.data, mask=args.mask,
                   diff2std=args.xfm, output_dir=args.output, model=args.model, template_dir=args.template)

    elif args.func == 'glm':
        if len(args.subjectdirs) == 1:
            args.subjectdirs = glob.glob(args.subjectdirs[0])

        glm_from_cli(subjdirs=args.subjectdirs, output=args.output, mask=args.mask, evs=args.des, contrast=args.con)


def parse_args(argv):
    """
    Parses the commandline input
    :param argv: input string from commandline
    :return: arg namespce from argparse
    :raises: if the number of provided files do not match with other arguments
    """

    parser = argparse.ArgumentParser("WHIM: White matter HIerarchical Modelling.")
    subparsers = parser.add_subparsers(dest='commandname', title='Available subcommands',
                                       help='Type whim <subcommand> --help to get help for each subcommand.')

    subject_parser = subparsers.add_parser('subject', help='Estimate subject parameters using Maximum a posteriori.')
    group_parser = subparsers.add_parser('group', help='Estimate group average given subjects parameter estimates.')
    em_parser = subparsers.add_parser('EM', help='Create model template by iterating over subject and group.')
    template_parser = subparsers.add_parser('template', help='Create fixel template given model templates.')
    fixel_parser = subparsers.add_parser('fixel', help='Fit fixel template to a new subjects.')
    glm_parser = subparsers.add_parser('glm', help='Run GLM on the fixel specific parameters')

    available_models = ['bas', 'bas_diso', 'bas_dperp']
    nf_type = lambda s: int(s) if s in ['1', '2', '3'] else os.path.abspath(s)

    # subject  arguments:
    subject_parser.add_argument("-d", "--data", help="dMRI data", required=True)
    subject_parser.add_argument("-b", "--bvals", help="b-value", required=True)
    subject_parser.add_argument("-r", "--bvecs", help="gradient directions", required=True)
    subject_parser.add_argument("-m", "--mask", help="Mask indicating which voxels to analyse", required=True)
    subject_parser.add_argument("--nf", help="Number of crossing fibres for each voxel. Either a scalar or an image.", required=True, type=nf_type)
    subject_parser.add_argument("--output", help="Output directory", required=True)

    subject_parser.add_argument("--xfm", help="Transformation from data to mask/prior space (default: identity transform)", required=False)
    subject_parser.add_argument("--init", required=False, help="Initial vale for fitting the parameters. Either default or path to a directory containing parameters.", default="default")
    subject_parser.add_argument("--prior", help="Directory with the prior mean and variance", required=False)

    subject_parser.add_argument("--model", help=f"Crossing fibre model. Any of: {available_models}", choices=available_models, required=False, default="bas")
    subject_parser.add_argument("--alpha", default=1.0, help="Shape parameter for the prior distribution on noise variance", required=False)
    subject_parser.add_argument("--beta", default=0.0, help="Scale parameter for the prior distribution on noise variance", required=False)
    subject_parser.add_argument('--jac', action='store_true', help="Use analytic estimation of the jacobian for optimisation")
    subject_parser.add_argument('--includeall', action='store_true', help="Include all the parameters in the hierarchy. By default only fibre directions are included.")
    subject_parser.set_defaults(func='subject')

    # group arguments:
    group_parser.add_argument("--subjdirs", nargs='+', help="List of subject output directories", required=True)
    group_parser.add_argument("--mask", help="Mask in standard space", required=True)
    group_parser.add_argument("--output", help="Output directory with group-level parameter means and variances", required=True)
    group_parser.add_argument("--nf", help='Number of crossing fibres in each voxel.', required=True, type=int)
    group_parser.add_argument("--alpha", default=-3.0, help='Shape parameter for the variance prior', required=False)
    group_parser.add_argument("--beta", default=0.0, help='Scale parameter for the variance prior', required=False)
    group_parser.set_defaults(func='group')

    # EM arguments:
    em_parser.add_argument("--file-tree", help="Description of input and optionally output data", required=True)
    em_parser.add_argument("--mask", help="Mask in standard space", required=True)
    em_parser.add_argument("--model", help=f"Diffusion model. Any of: \n{available_models}", choices=available_models, default='bas')
    em_parser.add_argument("--nf", help="Number of crossing fibres in each voxel.", required=True, type=int)
    em_parser.add_argument('--iters', default=5, help="Number of iterations.", required=False, type=int)
    em_parser.add_argument('--jac', action='store_true')
    em_parser.add_argument('--output', help="Output top-level directory.", required=True)
    em_parser.add_argument("-m", "--pipeline_method", default=RunMethod.default().name,
                           choices=[m.name for m in RunMethod],
                           help=f"Method used to run the jobs (default: {RunMethod.default().name})")
    em_parser.add_argument('--maxjobtime', help="Maximum time (in minutes) for fitting per subject. Adjust based on the number of voxels.", required=False, default=300)
    em_parser.add_argument("-noisealpha", default=1.0, help='Shape parameter for the noice variance prior', required=False)
    em_parser.add_argument("-noisebeta", default=0.0, help='Scale parameter for the noise variance prior', required=False)
    em_parser.add_argument("-groupalpha", default=-3.0, help='Shape parameter for the group variance prior', required=False)
    em_parser.add_argument("-groupbeta", default=0.0, help='Scale parameter for the group variance prior', required=False)
    em_parser.add_argument('--includeall', action='store_true', help="Include all the parameters in the hierarchy. By default only fibre directions are included.")

    em_parser.set_defaults(func='EM')

    # template arguments:
    template_parser.add_argument("--modeldirs", nargs='+', required=True,
                                 help="List of model template directories in order (bas1, bas2, bas2). "
                                      "The same format as whim group output.")
    template_parser.add_argument("--mask", help="Mask in standard space", required=True)
    template_parser.add_argument("--criterion", required=False, type=str, default='BIC',
                                 help="The criterion for model selection. Any of 'BIC'(default), 'AIC', 'LogLikelihood'.")
    template_parser.add_argument('--thresh', help="Threshold for the criterion differences. Default=(0, 0)",
                                 required=False, nargs=2, type=float, default=(0., 0.))

    template_parser.add_argument("--output", help="Output directory", required=True)
    template_parser.set_defaults(func="template")

    # fit fixels arguments:
    fixel_parser.add_argument("--template", required=True,
                              help="Template directory containing prior images per parameter.")
    fixel_parser.add_argument("--mask", help="Mask in standard space", required=True)
    fixel_parser.add_argument("--output", help="Output directory", required=True)
    fixel_parser.add_argument("--data", help="dMRI data", required=True)
    fixel_parser.add_argument("--bvals", help="bvalue file in fsl format", required=True)
    fixel_parser.add_argument("--bvecs", help="gradient directions file in fsl format", required=True)
    fixel_parser.add_argument("--xfm", help="Transformation from data to template space", required=True)
    fixel_parser.add_argument("--model", help=f"Crossing fibre model. Any of: {available_models}",
                              choices=available_models, required=False, default="bas_dperp")
    fixel_parser.set_defaults(func="fixel")

    # glm arguments:
    glm_parser.add_argument("--subjectdirs", nargs='+', help="Subjects fixel parameters directories", required=True)
    glm_parser.add_argument("--mask", help="Mask in standard space", required=True)
    glm_parser.add_argument("--des", required=True, help="Explanatory variables for GLM. A csv file, one EV per column.")
    glm_parser.add_argument("--con", help="Contrasts.", required=True)
    glm_parser.add_argument("--output", help="Output directory", required=True)

    glm_parser.set_defaults(func="glm")

    args = parser.parse_args(argv)
    if not hasattr(args, "func"):
        parser.print_help()
        exit(2)

    return args


def load_diff(diff_data, bvals, bvecs, mapping, bval_scale: int = 1000, bval_thr: float = 0.1):
    """
    Loads diffusion data, scales b-values, and handles bad voxels.

    Args:
        diff_data: Diffusion data file.
        bvals: File containing b-values.
        bvecs: File containing b-vectors.
        mapping: Mapping object for loading native data.
        bval_scale: Scaling factor for b-values.
        bval_thr: Threshold for identifying b0 images.

    Returns:
        Tuple of (data, bvals, bvecs, mean_b0, bad_voxels)
    """
    data = mapping.load_native(diff_data)
    bvals = np.genfromtxt(bvals) / bval_scale
    bvecs = np.genfromtxt(bvecs).T
    mean_b0 = np.mean(data[:, bvals < bval_thr], axis=1)
    bad_voxels = (data < 0).any(axis=1) | (data == 0).all(axis=1) | (mean_b0 == 0)  # Identify bad voxels

    mean_b0[bad_voxels] = 1.0
    return data, bvals, bvecs, mean_b0, bad_voxels


def load_template_params(template_dir: str, mapping: image_io.NNMapping, param_names: List[str], angle_idx: List[Tuple[int, int]]):
    """
    Loads prior images from the given directory.

    Args:
        template_dir: Directory containing template files.
        mapping: Mapping object for loading standard data.
        param_names: List of parameter names to load.
        angle_idx: List of angle indices (pairs) to process dyads.

    Returns:
        Tuple of nu_g and sigma2_g arrays containing mean and variance values.
    """
    if not os.path.exists(template_dir):
        raise ValueError(f"template directory {template_dir} does not exist.")
    
    n_vox = mapping.n_valid_vox
    n_params = len(param_names)
    nu_g, sigma2_g = np.zeros((n_vox, n_params)), np.zeros((n_vox, n_params))
    param_exists = False
    for i, pname in enumerate(param_names):
        mean_file = f'{template_dir}/{pname}_mean.nii.gz'
        var_file = f'{template_dir}/{pname}_var.nii.gz'

        if i not in angle_idx:
            if not os.path.exists(mean_file):
                warnings.warn(f'{pname} mean does not exist in the priors.')
            else:
                nu_g[:, i] = mapping.load_std(mean_file)
                param_exists = True
            if not os.path.exists(var_file):
                warnings.warn(f'{pname} variance does not exist in the priors.')
            else:
                sigma2_g[:, i] = mapping.load_std(var_file)

    for i, (ph_idx, th_idx) in enumerate(angle_idx, 1):
        dyad_mean_file = f'{template_dir}/dyads_{i}_mean.nii.gz'
        dyad_var_file = f'{template_dir}/dyads_{i}_var.nii.gz'
        
        if not os.path.exists(dyad_mean_file):
            warnings.warn(f'{dyad_mean_file} does not exist in the priors.')
        else:
            dyads = mapping.load_std(dyad_mean_file)
            nu_g[:, ph_idx], nu_g[:, th_idx] = utils.c2p(dyads)
            param_exists = True

        if not os.path.exists(dyad_var_file):
            warnings.warn(f'{dyad_var_file} does not exist in the priors.')
        else:
            sigma2_g[:, ph_idx] = mapping.load_std(dyad_var_file)
            sigma2_g[:, th_idx] = sigma2_g[:, ph_idx]

    if not param_exists:
        raise FileNotFoundError(f"No parameters are loaded from the template directory {template_dir}.")
    return nu_g, sigma2_g


def load_template_angles(template_dir: str, mapping: image_io.NNMapping):
    """
    Loads prior images from the given directory.

    Args:
        template_dir: Directory containing template files.
        mapping: Mapping object for loading standard data.
        param_names: List of parameter names to load.
        angle_idx: List of angle indices (pairs) to process dyads.

    Returns:
        Tuple of nu_g and sigma2_g arrays containing mean and variance values.
    """
    n_vox = mapping.n_valid_vox
    param_names = [f"dyads_{i+1}" for i in range(3)]
    nu_g, sigma2_g = np.zeros((n_vox, 3, 2)), np.zeros((n_vox, 3, 2))

    for i, pname in enumerate(param_names):
        mean_file = f'{template_dir}/{pname}_mean.nii.gz'
        var_file = f'{template_dir}/{pname}_var.nii.gz'
        dyads = mapping.load_std(mean_file)
        dispersion = mapping.load_std(var_file)
        nu_g[:, i, 0], nu_g[:, i, 1] = utils.c2p(dyads)
        sigma2_g[:, i, 0] = dispersion
        sigma2_g[:, i, 1] = dispersion

    return nu_g, sigma2_g


def load_subject_params(subject_dir: str, mapping: image_io.NNMapping, param_names: List[str]) -> np.ndarray:
    """
    Loads subject parameters from the specified directory.

    Args:
        subject_dir (str): Directory containing subject parameters.
        mapping (image_io.NNMapping): Mapping object for loading standard space data.
        param_names (List[str]): List of parameter names to load.

    Returns:
        np.ndarray: Array containing subject parameter values, stacked along axis 1.
    """
    params = []
    n_vox = mapping.n_valid_vox

    for pname in param_names:
        fname = os.path.join(subject_dir, f'{pname}_std.nii.gz')
        if os.path.exists(fname):
            x = mapping.load_std(fname)
        else:
            logging.warning(f"Parameter '{pname}' does not exist in directory: {subject_dir}")
            # Fill with NaN if the parameter file doesn't exist
            x = np.full((n_vox,), np.nan)

        params.append(x)

    # Stack the parameters along axis 1
    params = np.stack(params, axis=1)

    # Check if all parameters are NaN
    if np.all(np.isnan(params)):
        raise ValueError(f"No valid parameters were loaded from {subject_dir}")

    return params


def write_subject_params(output: str, mapping, nu_s: np.ndarray, param_names: List[str],
                         angle_idx: List[Tuple[int, int]], ll: np.ndarray, aic: np.ndarray, bic: np.ndarray,
                         write_standard: bool = True):
    """
    Writes subject fit results to the output directory.

    Args:
        output: Output directory.
        mapping: Mapping object for writing data.
        nu_s: Fitted parameter means.
        param_names: List of parameter names.
        angle_idx: List of angle indices for dyads.
        ll: Log-likelihood array.
        aic: AIC array.
        bic: BIC array.
        write_standard: Whether to write in standard space.

    """
    os.makedirs(output, exist_ok=True)

    # Align dyads
    dyads = []
    for a in angle_idx:
        nu_s[:, a[0]], nu_s[:, a[1]] = utils.align2avg(nu_s[:, a[0]], nu_s[:, a[1]])
        dyads.append(utils.p2c(nu_s[:, a[0]], nu_s[:, a[1]]))

    # Write native space parameters
    for mean_param, param_name in zip(nu_s.T, param_names):
        mapping.write_native(mean_param, f'{output}/{param_name}_native.nii.gz')

    for i, dyad in enumerate(dyads):
        mapping.write_native(dyad, f'{output}/dyads_{i + 1}_native.nii.gz')

    # Write fit quality metrics
    for metric, name in zip([ll, aic, bic], ['LogLikelihood', 'AIC', 'BIC']):
        mapping.write_native(metric, f'{output}/{name}_native.nii.gz')

    # Write standard space data
    if write_standard:
        for i, param in enumerate(param_names):
            mapping.write_std(nu_s[:, i], f'{output}/{param}_std.nii.gz')

        for i, dyad in enumerate(dyads):
            rotated_dyad = utils.rotate(dyad, mapping.valid_rot_mats)
            mapping.write_std(rotated_dyad, f'{output}/dyads_{i + 1}_std.nii.gz')

        for metric, name in zip([ll, aic, bic], ['LogLikelihood', 'AIC', 'BIC']):
            mapping.write_std(metric, f'{output}/{name}_std.nii.gz')


def fit_subject(bvals: In, bvecs: In, dmri: In, output_dir: Out,
                mask: str, n_sticks: int, model: str, includeall: bool,
                a_n: float, b_n: float, jac: bool,
                diff2std: In = None, prior: Ref = None, init: Ref = 'default',
                previous_model_group_dir: In = None,
                previous_model_gorup_file: In = None,
                output_file: Out = None):
    """
    Fits diffusion model parameters for a single subject.

    Args:
        bvals (In): Path or input object for b-values.
        bvecs (In): Path or input object for b-vectors.
        dmri (In): Diffusion-weighted data.
        mask (In): Brain mask.
        diff2std(In): Transformation between native and standard space, if available.
        output_dir (Out): Directory to store the output files.
        n_sticks: Number of fibers per voxel or an image specifying this.
        prior: Priors directory for the fitting process, if any.
        init: Initial parameter values, if available.
        model: The diffusion model class.
        includeall (bool): Whether to include all parameters in the fitting.
        a_n (float): Alpha parameter for the noise prior.
        b_n (float): Beta parameter for the noise prior.
        jac (bool): Whether to use Jacobian for fitting.
        previous_model_group_dir: dummy input for fsl_pipe job management.
        previous_model_gorup_file: dummy input for fsl_pipe job management.
        previous_model_gorup_file: dummy input for fsl_pipe job management.
        output_file: dummy output for fsl_pipe job management
    """
    start = timer()
    logging.info('Starting parameter fitting for subject.')

    # Model selection dictionary
    model_map = {
        'bas': diffusion_models.BallStick,
        'bas_diso': diffusion_models.BallStick_DISO
    }

    # Check if the model is valid
    if model not in model_map:
        raise ValueError(f"Model {model} is not defined. Available models are: {list(model_map.keys())}")

    # Select the model
    diffmodel = model_map[model]

    # Compute mapping between native and standard space
    logging.info('Compute mapping between native and standard space.')
    mapping = image_io.NNMapping(dmri, mask, diff2std)

    # Load diffusion data
    logging.info('Loading diffusion data.')
    data, bvals, bvecs, mean_b0, bad_voxels = load_diff(dmri, bvals, bvecs, mapping)
    data /= mean_b0[:, np.newaxis]
    n_vox = data.shape[0]
    fine_voxels = ~bad_voxels
    logging.info(f'Loaded data from {fine_voxels.sum()} valid voxels.')

    # Handle n_sticks input (either an integer or a file path)
    if isinstance(n_sticks, int):
        nf = np.full(n_vox, n_sticks)
    elif os.path.exists(n_sticks):
        nf = mapping.load_std(n_sticks)
    else:
        raise ValueError("n_sticks must be either an integer or a valid file path.")

    # Initialize the full model with 3 sticks
    full_model = diffmodel(n_sticks=3, bvals=bvals, bvecs=bvecs)
    s0_idx = full_model.param_names.index("s_0")

    # Load prior if provided
    if prior is not None:
        logging.info('Loading priors for MAP estimation.')
        nu_g, sigma2_g = load_template_params(prior, mapping, full_model.param_names, full_model.angle_idx)
        nu_g[:, s0_idx] /= mean_b0

    # Initialize parameters based on init strategy
    if init == 'groupmean' and prior is not None:
        x0 = nu_g
    elif init == 'default':
        logging.info('Generating default initial values for parameters.')
        x0 = np.zeros((n_vox, full_model.n_params))
        for i in range(1, 4):
            mdl = diffmodel(n_sticks=i, bvals=bvals, bvecs=bvecs)
            x0[nf == i, :mdl.n_params] = mdl.x0()
    elif os.path.exists(init):
        logging.info('Loading initial values for parameters from directory.')
        x0 = load_subject_params(init, mapping, full_model.param_names)
        x0[:, s0_idx] /= mean_b0
    else:
        raise ValueError("init must be 'groupmean' (when prior is provided), 'default', or a valid directory path with <param>_std.nii.gz.")

    # Create a parameter mask based on includeall
    param_mask = np.ones(full_model.n_params, dtype=bool) if includeall else np.zeros(full_model.n_params, dtype=bool)
    if not includeall:
        for phi_idx, th_idx in full_model.angle_idx:
            param_mask[phi_idx] = True
            param_mask[th_idx] = True

    # Allocate space for results
    nu_s = np.full((n_vox, full_model.n_params), np.nan)
    ll, aic, bic = (np.full(n_vox, np.nan) for _ in range(3))

    # Fit the model for each number of sticks (1 to 3)
    for i in range(1, 4):
        idx = (nf == i) & fine_voxels
        if np.any(idx):
            logging.info(f'Fitting model with {i} sticks to {np.sum(idx)} voxels.')
            mdl = diffmodel(n_sticks=i, bvals=bvals, bvecs=bvecs)
            n_params = mdl.n_params
            hm = hbm.HierarchicalModel.from_diff_model(mdl, noise_priors=(a_n, b_n), param_mask=param_mask[:n_params])

            if prior is None:
                nu_s[idx, :n_params] = hm.fit_mse(data[idx], x0[idx, :n_params], parallel=False)
            else:
                nu_s[idx, :n_params], _ = hm.fit_map(data[idx], nu_g[idx, :n_params], sigma2_g[idx, :n_params],
                                                     x0[idx, :n_params], mapping.valid_rot_mats[idx], parallel=False, use_jac=jac)
            ll[idx], aic[idx], bic[idx] = hm.calc_quality(data[idx], nu_s[idx, :n_params])

    # Rescale S0 parameter
    nu_s[:, s0_idx] *= mean_b0

    # Write results to disk
    logging.info("Writing results to disk.")
    write_subject_params(output_dir, mapping, nu_s, full_model.param_names, full_model.angle_idx, ll, aic, bic,
                         write_standard=(diff2std is not None))

    # Log the total time taken
    end = timer()
    logging.info(f"Fitting completed in {timedelta(seconds=end - start)}.")


def fit_fixel(bvals: In, bvecs: In, dmri: In, mask: str,  diff2std: In, template_dir: In, output_dir: Out, model: str):
    """
    Fits diffusion model parameters for a single subject.

    Args:
        bvals (In): Path or input object for b-values.
        bvecs (In): Path or input object for b-vectors.
        dmri (In): Diffusion-weighted data.
        mask (In): Brain mask.
        diff2std(In): Transformation between native and standard space, if available.
        output_dir (Out): Directory to store the output files.
        model: The diffusion model class.
    """
    start = timer()
    logging.info('Starting parameter fitting for subject.')

    # Model selection dictionary
    model_map = {
        'bas': diffusion_models.BallStick,
        'bas_diso': diffusion_models.BallStick_DISO,
        'bas_dperp': diffusion_models.BallStick_PerpendicularDiffusivity,
    }

    # Check if the model is valid
    if model not in model_map:
        raise ValueError(f"Model {model} is not defined. Available models are: {list(model_map.keys())}")
    else:
        diffmodel = model_map[model]

    # Compute mapping between native and standard space
    logging.info('Compute mapping between native and standard space.')
    mapping = image_io.NNMapping(dmri, mask, diff2std)

    # Load diffusion data
    logging.info('Loading diffusion data.')
    data, bvals, bvecs, mean_b0, bad_voxels = load_diff(dmri, bvals, bvecs, mapping)
    data /= mean_b0[:, np.newaxis]
    n_vox = data.shape[0]
    fine_voxels = ~bad_voxels
    logging.info(f'Loaded data from {fine_voxels.sum()} valid voxels.')

    # Handle
    n_sticks = os.path.join(template_dir, 'n_fibres.nii.gz')
    if os.path.exists(n_sticks):
        nf = mapping.load_std(n_sticks)
    else:
        raise ValueError("Template does not contain n_fibres.nii.gz")

    # Initialize the full model with 3 sticks
    full_model = diffmodel(n_sticks=3, bvals=bvals, bvecs=bvecs)
    s0_idx = full_model.param_names.index("s_0")
    logging.info('Loading angle priors.')
    angle_means, angle_vars = load_template_angles(template_dir, mapping)
    nu_g = np.tile([full_model.priors[p].mean() for p in full_model.param_names], (n_vox, 1))
    sigma2_g = np.tile([full_model.priors[p].var() for p in full_model.param_names], (n_vox, 1))
    for i, (phi_idx, th_idx) in enumerate(full_model.angle_idx):
        nu_g[:, phi_idx] = angle_means[:, i, 0]
        nu_g[:, th_idx] = angle_means[:, i, 1]
        sigma2_g[:, phi_idx] = angle_vars[:, i, 0]
        sigma2_g[:, th_idx] = angle_vars[:, i, 1]


    logging.info('Setting initial values for parameters.')
    x0 = np.zeros((n_vox, full_model.n_params))
    for i in range(1, 4):
        mdl = diffmodel(n_sticks=i, bvals=bvals, bvecs=bvecs)
        x0[nf == i, :mdl.n_params] = mdl.x0()

    param_mask = np.ones(full_model.n_params, dtype=bool)
    nu_s = np.full((n_vox, full_model.n_params), np.nan)
    ll, aic, bic = (np.full(n_vox, np.nan) for _ in range(3))

    # Fit the model for each number of sticks (1 to 3)
    for i in range(1, 4):
        idx = (nf == i) & fine_voxels
        if np.any(idx):
            logging.info(f'Fitting model with {i} sticks to {np.sum(idx)} voxels.')
            mdl = diffmodel(n_sticks=i, bvals=bvals, bvecs=bvecs)
            n_params = mdl.n_params
            hm = hbm.HierarchicalModel.from_diff_model(mdl, noise_priors=(1, 0), param_mask=param_mask[:n_params])
            nu_s[idx, :n_params], _ = hm.fit_map(data[idx], nu_g[idx, :n_params], sigma2_g[idx, :n_params],
                                                     x0[idx, :n_params], mapping.valid_rot_mats[idx], parallel=False)
            ll[idx], aic[idx], bic[idx] = hm.calc_quality(data[idx], nu_s[idx, :n_params])

    # Rescale S0 parameter
    nu_s[:, s0_idx] *= mean_b0

    # Write results to disk
    logging.info("Writing results to disk.")
    write_subject_params(output_dir, mapping, nu_s, full_model.param_names, full_model.angle_idx, ll, aic, bic,
                         write_standard=(diff2std is not None))

    # Log the total time taken
    end = timer()
    logging.info(f"Fitting completed in {timedelta(seconds=end - start)}.")


def get_param_names(subject_dirs: List[str]) -> List[str]:
    """
    Get the parameter names that are common across all subject directories. Assumes the format <param>_std.nii.gz.
    Args:
        subject_dirs (list): List of subject directories containing parameter files.

    Returns:
        list: List of common parameter names across all subject directories.
    """

    param_files = set([os.path.basename(p) for p in glob.glob(os.path.join(subject_dirs[0], '*_std.nii.gz'))])
    bad_subjects = []
    for subject_dir in subject_dirs[1:]:
        current_param_files = set([os.path.basename(p) for p in glob.glob(os.path.join(subject_dir, '*_std.nii.gz'))])
        if len(current_param_files):
            param_files.intersection_update(current_param_files)
        else:
            bad_subjects.append(subject_dir)
            warnings.warn(f"There is no parameter estimate file in {subject_dir}")

    param_names = [p.split('_std')[0] for p in param_files]
    param_names.sort(key=lambda x: x[-1])
    return param_names, bad_subjects


def estimate_group(subject_dirs: In, output_dir: Out, mask: str, a_g: float, b_g: float, nf=None, output_file: Out=None, input_file: In=None):
    """
    Estimate group-level parameters given subject parameters.

    Args:
        subject_dirs (list): List of directories that include subject parameter fits in standard space.
        output_dir (str): Output directory to save group estimates.
        mask (str): Path to the mask in standard space.
        a_g (float): Shape parameter for group variance prior.
        b_g (float): Scale parameter for group variance prior.
        nf (int): Number of crossing fibres in each voxel
        output_file: dummy output for fsl_pipe job management.
        input_file: dummy input for fsl_pipe file management

    """
    # Handle xarray input from fsl_pipe
    subject_dirs = subject_dirs.data if hasattr(subject_dirs, 'data') else subject_dirs
    # handle wildcards
    expanded_files = []
    for d in subject_dirs:
        expanded_files.extend(glob.glob(d))
    subject_dirs = [os.path.abspath(f) for f in expanded_files]

    # Get common parameter names
    param_names, bad_subjects = get_param_names(subject_dirs)
    fit_quality_names = ('LogLikelihood', 'AIC', 'BIC')
    param_names = [p for p in param_names if p not in fit_quality_names]
    if len(param_names) == 0:
        raise ValueError("No parameters were found in any of the subject parameter directories.")

    avail_subjs = [s for s in subject_dirs if s not in bad_subjects]  # exclude subjects that are missing parameter estimates.
    print(f'Updating group parameters {param_names} with {len(avail_subjs)} subjects.')

    nu_g, sigma2_g = {}, {}
    _, _, src_coords = image_io.read_image(mask, mask)

    # Calculate parameter averages
    for p in param_names:
        nu_s = [image_io.read_image(os.path.join(s, f'{p}_std.nii.gz'), mask)[0] for s in avail_subjs]
        nu_s = np.stack(nu_s, axis=1)
        n_vox = nu_s.shape[0]

        # Determine whether to use dyadic or scalar averaging
        avg_func = hbm.average_dyads if 'dyads' in p else hbm.average_scalar
        if np.any(~np.isnan(nu_s)) and np.any(nu_s != 0):
            nu_g[p], sigma2_g[p] = utils.run_parallel(lambda v: avg_func(nu_s[v], a_g, b_g), n_vox, debug=True)

    # Sort fibers according to signal fractions (specific to the Ball and Stick model.)
    param_names = list(nu_g.keys())
    if nf is None:
        nf = len([p for p in param_names if 'dyads' in p])

    fs = np.stack([nu_g[f'f_{i + 1}'] for i in range(nf)], axis=0)
    idx = np.argsort(fs, axis=0)[::-1]

    for vox in range(n_vox):
        tmp_nu = {p: nu_g[p][vox].copy() for p in param_names}
        tmp_sigma = {p: sigma2_g[p][vox].copy() for p in param_names}
        for i in range(nf):
            for p in ['f', 'dyads']:
                nu_g[f'{p}_{i + 1}'][vox] = tmp_nu[f'{p}_{idx[i, vox] + 1}']
                sigma2_g[f'{p}_{i + 1}'][vox] = tmp_sigma[f'{p}_{idx[i, vox] + 1}']

    # Write group means and variances to NIfTI files
    os.makedirs(output_dir, exist_ok=True)
    for p in param_names:
        image_io.write_nifti(nu_g[p], src_coords, mask, os.path.join(output_dir, f'{p}_mean.nii.gz'))
        image_io.write_nifti(sigma2_g[p], src_coords, mask, os.path.join(output_dir, f'{p}_var.nii.gz'))

    # Summarize and write group quality metrics (LogLikelihood, AIC, BIC)
    for q_name in fit_quality_names:
        q = [image_io.read_image(os.path.join(s, f'{q_name}_std.nii.gz'), mask)[0] for s in subject_dirs]
        q_sum = np.stack(q, axis=0).sum(axis=0)
        image_io.write_nifti(q_sum, src_coords, mask, os.path.join(output_dir, f'{q_name}.nii.gz'))

    print('Group parameter estimation completed.')


def expectation_maximisation(file_tree: str, output: str, mask: str, model: str, nf: int, max_iters: int, jac: bool, pipeline_method: str,
                             a_n: float, b_n:float, a_g:float, b_g: float, maxjobtime):
    """
    Fits the hierarchical model to the data using expectation maximisation.

    Args:
        file_tree (str): Path to the file tree configuration.
        output (str): Output directory for the results.
        mask (str): Path to the brain mask file.
        model (str): The name of the diffusion model to be used.
        nf (int): Number of fibres (n_f).
        max_iters (int): Number of iterations for the EM algorithm.
        jac (bool): Whether to use Jacobian correction.
        pipeline_method: running method for fsl_pipe (local, submit, dask)
        maxjobtime: Maximum time for the fitting time to finish. Use larger numbers for bigger mask to avoid shutting down the job.
    """
    model_name = model + str(nf)
    tree = FileTree.read(file_tree).fill().update_glob("dmri")
    output_tree_path = str(importlib.resources.files('whim.trees').joinpath("whim_output.tree"))
    output_tree = FileTree.read(output_tree_path, top_level=output).update(model=model_name)

    tree.add_subtree(output_tree, parent=None)
    tree.placeholders[("iter", "previous_iter")] = [range(1, max_iters+1), range(0, max_iters)]

    fit_args = dict(mask=mask, n_sticks=nf, model=model, includeall=False, a_n=a_n, b_n=b_n, jac=jac)
    group_args = dict(mask=mask, a_g=a_g, b_g=b_g, nf=nf)
    pipe = Pipeline()
    pipe(fit_subject, submit=dict(jobtime=maxjobtime, logdir=Out('model_subject_dir')),
         placeholders=dict(iter=0, previous_iter=0),
         kwargs=dict(output_dir=Out('model_subject_dir'),
                     previous_model_group_dir=None,
                     previous_model_gorup_file=None,
                     prior=None,
                     init='default',
                     output_file=Out('subject_f1'),
                     **fit_args))  # initial MLE

    pipe(estimate_group, submit=dict(jobtime=200, logdir=Out('model_group_dir')),
         no_iter=['subject'],
         placeholders=dict(iter=0, previous_iter=0),
         kwargs=dict(subject_dirs=In('model_subject_dir'),
                     input_file=In('subject_f1'),
                     output_dir=Out('model_group_dir'),
                     output_file=Out('group_f1'),
                     **group_args))

    pipe(fit_subject, submit=dict(jobtime=maxjobtime, logdir=Out('model_subject_dir')),
         kwargs=dict(output_dir=Out('model_subject_dir'),
                     previous_model_gorup_file=In('previous_group_f1'),
                     prior=Ref('previous_model_group_dir'),
                     init='groupmean',
                     output_file=Out('subject_f1'),
                     **fit_args))

    pipe(estimate_group, submit=dict(jobtime=200, logdir=Out('model_group_dir')),
         no_iter=['subject'],
         kwargs=dict(subject_dirs=In('model_subject_dir'),
                     output_dir=Out('model_group_dir'),
                     output_file=Out('group_f1'),
                     input_file=In('subject_f1'),
                     **group_args))

    jobs = pipe.generate_jobs(tree)
    graph = pipe.add_to_graph(tree=tree)
    graph.render(f"{tree.top_level}/pipeline")
    jobs = jobs.filter()  # remove already successful jobs.
    jobs.run(pipeline_method)

    logging.info("Expectation maximisation completed.")


def make_template(modeldirs, criterion, mask, thresholds, output):
    """
    gets results from different models and selects the right number of fibres in each voxel
    Assumes models are given in the order  (1, 2, 3) fibres.
    """
    q = [image_io.read_image(f'{m}/{criterion}.nii.gz', mask)[0] for m in modeldirs]
    if criterion == "LogLikelihood":
        m21 = q[1] - q[0]
        m32 = q[2] - q[1]
        winning_idx = np.zeros_like(q[0])
        winning_idx[m21 > thresholds[0]] = 1
        winning_idx[np.logical_and(m32 > thresholds[1], m21 > thresholds[1])] = 2
    else:
        winning_idx = np.argmin(np.stack(q, axis=0), axis=0)

    _, _, src_coords = image_io.read_image(mask, mask)
    n_vox = len(src_coords)
    os.makedirs(output, exist_ok=True)

    # save n_fibres to disk assuming models are given in correct order (bas1, bas2 , bas 3).
    nf = winning_idx + 1
    image_io.write_nifti(nf, src_coords, mask, output + '/n_fibres.nii.gz')

    # load all template files and create a single template.
    param_names = [p.split('/')[-1].split('_mean')[0] for p in glob.glob(modeldirs[-1] + '/*_mean.nii.gz')]
    param_names = [p for p in param_names if p not in ('LogLikelihood', 'AIC', 'BIC')]

    for p in param_names:
        all_means, all_vars = [], []
        n_dim = 3 if "dyad" in p else 1
        for m_idx, m in enumerate(modeldirs):
            if os.path.exists(f'{m}/{p}_mean.nii.gz'):
                all_means.append(image_io.read_image(f'{m}/{p}_mean.nii.gz', mask)[0])
                all_vars.append(image_io.read_image(f'{m}/{p}_var.nii.gz', mask)[0])
            else:
                all_means.append(np.squeeze(np.zeros((n_vox, n_dim))))
                all_vars.append(np.zeros((n_vox,)))

        template_mean = np.zeros_like(all_means[-1])
        template_var = np.zeros(n_vox)
        for w in range(3):
            idx = winning_idx == w
            template_mean[idx] = all_means[w][idx]
            template_var[idx] = all_vars[w][idx]

        image_io.write_nifti(template_mean, src_coords, mask, f'{output}/{p}_mean.nii.gz')
        image_io.write_nifti(template_var, src_coords, mask, f'{output}/{p}_var.nii.gz')
    logging.info("Template is successfully created.")


def glm_from_cli(subjdirs, output, mask, evs, contrast):
    param_names, bad_subjs = get_param_names(subjdirs)
    bad_names = np.array([[f"{p}_{i}" for p in ['dyads', 'phi', 'theta']] for i in range(1, 4)]).flatten()
    param_names = [item for item in param_names if item not in bad_names]

    if len(param_names) == 0:
        raise ValueError(f'No parameters found in subject directories. {subjdirs}')

    os.makedirs(output, exist_ok=True)
    _, _, src_coords = image_io.read_image(mask, mask)

    evs = np.genfromtxt(evs)
    if evs.shape[0] != len(subjdirs):
        raise ValueError(f'number of EV rows {evs.shape[0]} must match the number of subjects {len(subjdirs)}.')
    con = np.atleast_2d(np.genfromtxt(contrast))
    if evs.shape[1] != con.shape[1]:
        raise ValueError(f'Number of EV columns {evs.shape[1]} must match the number of Contrast columns {con.shape[1]}')

    logging.info(f'Running GLM for {len(subjdirs)} subjects on {param_names}.')

    for p in param_names:
        nu_s = []
        for s in subjdirs:
            f_name = f'{s}/{p}_std.nii.gz'
            nu_s.append(image_io.read_image(f_name, mask)[0])
        nu_s = np.stack(nu_s, axis=1)
        pe, cope, varcope, zscore = utils.glm(nu_s.T, evs, con)

        # write to file:
        image_io.write_nifti(pe.T, src_coords, mask, f'{output}/{p}_pe.nii.gz')
        image_io.write_nifti(cope.T, src_coords, mask, f'{output}/{p}_cope.nii.gz')
        image_io.write_nifti(varcope.T, src_coords, mask, f'{output}/{p}_varcope.nii.gz')
        image_io.write_nifti(zscore.T, src_coords, mask, f'{output}/{p}_zscore.nii.gz')
        logging.info(f"Done for {p}")
    logging.info(f'GLM is completed. Results are stored in {output}.')
